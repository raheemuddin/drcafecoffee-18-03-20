package com.cs.drcafe.Dialogs;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.drcafe.Activities.ItemsindividualActivity;
import com.cs.drcafe.R;
import com.xujiaji.happybubble.BubbleDialog;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class CustomDialog extends BubbleDialog implements View.OnClickListener {
    private ViewHolder mViewHolder;
    private OnClickCustomButtonListener mListener;
    String search;
    PopupWindow popupWindow;
    ArrayList<String> store_name = new ArrayList();
    ArrayList<String> store_address = new ArrayList();


    public CustomDialog(final Context context, ArrayList<String> store_name, ArrayList<String> store_address, int pos, final String address) {
        super(context);
        setTransParentBackground();
        setPosition(Position.TOP);
        View rootView = LayoutInflater.from(context).inflate(R.layout.custom_dialog, null);
        mViewHolder = new ViewHolder(rootView);
        addContentView(rootView);

        mViewHolder.store_name.setText("" + store_name.get(pos));
        mViewHolder.store_address.setText("" + store_address.get(pos));

        mViewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(context, ItemsindividualActivity.class);
                a.putExtra("address", address);
                context.startActivity(a);

            }
        });

    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onClick(((Button) v).getText().toString());
        }
    }

    private static class ViewHolder {
        //        EditText search_text;
        LinearLayout layout;
        TextView store_name, store_address;


        public ViewHolder(View rootView) {
            layout = (LinearLayout) rootView.findViewById(R.id.layout);

            store_name = (TextView) rootView.findViewById(R.id.store_name);
            store_address = (TextView) rootView.findViewById(R.id.store_address);

        }
    }

    public void setClickListener(OnClickCustomButtonListener l) {
        this.mListener = l;
    }

    public interface OnClickCustomButtonListener {
        void onClick(String str);
    }
}
