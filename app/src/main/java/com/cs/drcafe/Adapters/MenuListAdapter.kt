package com.cs.drcafe.Adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Activities.ItemsindividualActivity
import com.cs.drcafe.Models.StoreCategoryList
import com.cs.drcafe.R
import java.util.ArrayList

class MenuListAdapter(
    var context: Context,
    main_cat_list: ArrayList<StoreCategoryList.Category>?,
    data_list: ArrayList<StoreCategoryList.SubCategory>?,
    str_address: String
) : RecyclerView.Adapter<MenuListAdapter.MyViewHolder?>() {
    var inflater: LayoutInflater
    var main_cat_list: ArrayList<StoreCategoryList.Category>?
    var data_list: ArrayList<StoreCategoryList.SubCategory>?
    var cardViewList: List<TextView> = ArrayList()
    var str_address: String
    var pos: Int = 0
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View
//        if (language.equals("En", ignoreCase = true)) {
        itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.menu_list, parent, false)
//        } else {
//            itemView = LayoutInflater.from(parent.context)
//                .inflate(R.layout.driver_list_arabic, parent, false)
//        }
        //     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(
        @NonNull holder: MyViewHolder, position: Int
    ) {
        if (position == pos) {

            if (main_cat_list!![position].nameEn.equals("V12")) {

                holder.layout1.setBackgroundColor(context.resources.getColor(R.color.v12header))

            } else {

                holder.layout1.setBackgroundColor(context.resources.getColor(R.color.menu_selected))

            }

        } else {

            if (main_cat_list!![position].equals("V12")) {

                holder.layout1.setBackgroundColor(context.resources.getColor(R.color.v12header))

            } else {

                holder.layout1.setBackgroundColor(context.resources.getColor(R.color.white))

            }

        }

        holder.name.setText("" + main_cat_list!![position].nameEn)

//        if
        holder.cat_name.setText("")

        holder.img.visibility = View.INVISIBLE

//        holder.img.setImageDrawable(context.resources.getDrawable(cat_img[position]))


        if (main_cat_list!![position].nameEn!!.trim().equals("V12", true)) {

            holder.cat_name.setTextColor(context.resources.getColor(R.color.white))
            holder.name.setTextColor(context.resources.getColor(R.color.white))
            holder.layout1.setBackgroundColor(context.resources.getColor(R.color.v12header))

        } else {

            holder.cat_name.setTextColor(context.resources.getColor(R.color.text_color))
            holder.name.setTextColor(context.resources.getColor(R.color.text_color))
            holder.layout1.setBackgroundColor(context.resources.getColor(R.color.white))

        }

        holder.cart_layout.setOnClickListener {

            var str_sub_cat: String = ""

//            if (main_cat_list[position].equals("Discover")) {
//
//                str_sub_cat = "Featured"
//
//            } else if (main_cat_list[position].equals("Hot")) {
//
                str_sub_cat = "Beverage"
//
//            } else if (main_cat_list[position].equals("Cold")) {
//
//                str_sub_cat = "Beverage"
//
//            } else if (main_cat_list[position].equals("V12")) {
//
//                str_sub_cat = "V12"
//
//            } else if (main_cat_list[position].equals("Pastry")) {
//
//                str_sub_cat = "Food"
//
//            } else if (main_cat_list[position].equals("Sandwich")) {
//
//                str_sub_cat = "Beans"
//
//            }

            val a = Intent(context, ItemsindividualActivity::class.java)
            a.putExtra("address", str_address)
            a.putExtra("str_menu_name", str_sub_cat)
            context.startActivity(a)

            pos = position
            notifyDataSetChanged()

        }


    }

    inner class MyViewHolder(convertView: View) :
        RecyclerView.ViewHolder(convertView) {
        //        var rating: TextView
        //        var assign_cancel: TextView
//        var live_tracking: TextView
//        var driver_pic: CircleImageView
        var cart_layout: CardView
        var layout1: LinearLayout
        var img: ImageView
        var name: TextView
        var cat_name: TextView

        init {

            layout1 =
                convertView.findViewById<View>(R.id.layout1) as LinearLayout
            img =
                convertView.findViewById<View>(R.id.img) as ImageView
            name =
                convertView.findViewById<View>(R.id.names) as TextView
            cat_name =
                convertView.findViewById<View>(R.id.cat_names) as TextView
            cart_layout =
                convertView.findViewById<View>(R.id.card_layout) as CardView

            convertView.setOnClickListener(View.OnClickListener {

                //                pos = adapterPosition
                notifyDataSetChanged()

            })

            //            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, OrderTypeActivity.class);
//                    context.startActivity(a);
//
//                }
//            });
        }
    }


    init {
        this.main_cat_list = main_cat_list
        this.data_list = data_list
        this.str_address = str_address
        inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getItemCount(): Int {

        return main_cat_list!!.size

    }
}