package com.cs.drcafe.Adapters

import android.content.Context
import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Activities.ItemsindividualActivity
import com.cs.drcafe.R
import org.w3c.dom.Text

class V12SubItemListAdapter(
    var context: Context,
    sub_item_list: ArrayList<String> = ArrayList()
) : RecyclerView.Adapter<V12SubItemListAdapter.MyViewHolder?>() {
    var inflater: LayoutInflater
    var pos: Int = 0
    var sub_item_list: ArrayList<String> = ArrayList()
    lateinit var madapter: V12ItemListAdapter
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View
//        if (language.equals("En", ignoreCase = true)) {
        itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_sub_list, parent, false)
//        } else {
//            itemView = LayoutInflater.from(parent.context)
//                .inflate(R.layout.driver_list_arabic, parent, false)
//        }
        //     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(
        @NonNull holder: MyViewHolder, position: Int
    ) {

        if (position == 0) {

            holder.top_view.visibility = View.GONE
            holder.bottom_view.visibility = View.GONE

        } else if (position == (sub_item_list.size - 1)) {

            if (holder.item_list.visibility == View.VISIBLE) {

                holder.top_view.visibility = View.VISIBLE
                holder.bottom_view.visibility = View.GONE

            } else {

                holder.top_view.visibility = View.VISIBLE
                holder.bottom_view.visibility = View.VISIBLE

            }

        } else {

            holder.top_view.visibility = View.VISIBLE
            holder.bottom_view.visibility = View.GONE

        }

        if (sub_item_list[position].equals(ItemsindividualActivity.str_menu_name1)) {

            holder.item_list.visibility = View.VISIBLE
            holder.arrow.setImageDrawable(context.resources.getDrawable(R.drawable.right_arrow))

        } else {

            holder.item_list.visibility = View.GONE
            holder.arrow.setImageDrawable(context.resources.getDrawable(R.drawable.black_down_arrow))

        }

        holder.sub_item_name.text = sub_item_list[position]

        holder.sub_item_layout.setOnClickListener {

            if (holder.item_list.visibility == View.VISIBLE) {

                holder.item_list.visibility = View.GONE
                holder.arrow.setImageDrawable(context.resources.getDrawable(R.drawable.black_down_arrow))

            } else {

                holder.item_list.visibility = View.VISIBLE
                holder.arrow.setImageDrawable(context.resources.getDrawable(R.drawable.right_arrow))

            }

            ItemsindividualActivity.str_menu_name1 = sub_item_list[position]
            ItemsindividualActivity.headerListAdapter.notifyDataSetChanged()

            notifyDataSetChanged()

        }

        val linearLayoutManager1 = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        holder.item_list!!.setLayoutManager(linearLayoutManager1)
        madapter = V12ItemListAdapter(
            context
        )
        holder.item_list!!.setAdapter(madapter)

    }

    inner class MyViewHolder(convertView: View) :
        RecyclerView.ViewHolder(convertView) {

        var sub_item_layout: RelativeLayout
        var sub_item_name: TextView
        var item_list: RecyclerView
        var arrow: ImageView
        var top_view: View
        var bottom_view: View


        init {

            sub_item_layout = convertView.findViewById(R.id.sub_item_layout) as RelativeLayout
            sub_item_name = convertView.findViewById(R.id.sub_item_name) as TextView
            item_list = convertView.findViewById(R.id.items_list) as RecyclerView
            arrow = convertView.findViewById(R.id.arrow) as ImageView
            top_view = convertView.findViewById(R.id.top_view) as View
            bottom_view = convertView.findViewById(R.id.bottom_view) as View

            convertView.setOnClickListener(View.OnClickListener {

                //                pos = adapterPosition
                notifyDataSetChanged()

            })

            //            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, OrderTypeActivity.class);
//                    context.startActivity(a);
//
//                }
//            });
        }
    }


    init {

        this.sub_item_list = sub_item_list
        inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getItemCount(): Int {

        return sub_item_list.size

    }
}