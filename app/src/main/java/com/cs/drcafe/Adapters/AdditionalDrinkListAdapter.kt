package com.cs.drcafe.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Activities.ItemsindividualActivity.Companion.str_menu_name
import com.cs.drcafe.R
import java.util.*

class AdditionalDrinkListAdapter(
    var context: Context,
    sizeList: ArrayList<String>,
    priceList: ArrayList<String>
) : RecyclerView.Adapter<AdditionalDrinkListAdapter.MyViewHolder?>() {
    var inflater: LayoutInflater
    var sizeList: ArrayList<String> = ArrayList<String>()
    var priceList: ArrayList<String> = ArrayList<String>()
    var cardViewList: List<TextView> = ArrayList()
    var pos: Int = 1
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View
//        if (language.equals("En", ignoreCase = true)) {
        itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.add_drink_list, parent, false)
//        } else {
//            itemView = LayoutInflater.from(parent.context)
//                .inflate(R.layout.driver_list_arabic, parent, false)
//        }
        //     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(
        @NonNull holder: MyViewHolder, position: Int
    ) {

        if (position == 0) {

            holder.price.setBackgroundColor(context.resources.getColor(R.color.white))
            holder.size.setBackgroundColor(context.resources.getColor(R.color.white))

        } else if (position == pos) {

            holder.size.background =
                context.resources.getDrawable(R.drawable.add_drink_size_seleted)

            holder.price.background =
                context.resources.getDrawable(R.drawable.add_drink_price_seleted)

        } else {

            holder.price.setBackgroundColor(context.resources.getColor(R.color.white))
            holder.size.setBackgroundColor(context.resources.getColor(R.color.white))

        }

        holder.main_layout.setOnClickListener {


            pos = position
            notifyDataSetChanged()

        }

        holder.price.text = "" + priceList[position]
        holder.size.text = "" + sizeList[position]

    }

    inner class MyViewHolder(convertView: View) :
        RecyclerView.ViewHolder(convertView) {

        var size: TextView
        var price: TextView
        var main_layout: LinearLayout

        init {

            size = convertView.findViewById<View>(R.id.size) as TextView
            price =
                convertView.findViewById<View>(R.id.price) as TextView
            main_layout =
                convertView.findViewById<View>(R.id.main_layout) as LinearLayout

            convertView.setOnClickListener(View.OnClickListener {

                //                pos = adapterPosition
                notifyDataSetChanged()

            })

            //            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, OrderTypeActivity.class);
//                    context.startActivity(a);
//
//                }
//            });
        }
    }


    init {
        this.sizeList = sizeList
        this.priceList = priceList
        inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getItemCount(): Int {

        return priceList.size

    }
}