package com.cs.drcafe.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Activities.AdditionalActivity
import com.cs.drcafe.R

class ItemListAdapter(
    var context: Context
) : RecyclerView.Adapter<ItemListAdapter.MyViewHolder?>() {
    var inflater: LayoutInflater
    var pos: Int = 0
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View
//        if (language.equals("En", ignoreCase = true)) {
        itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list, parent, false)
//        } else {
//            itemView = LayoutInflater.from(parent.context)
//                .inflate(R.layout.driver_list_arabic, parent, false)
//        }
        //     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(
        @NonNull holder: MyViewHolder, position: Int
    ) {

    }

    inner class MyViewHolder(convertView: View) :
        RecyclerView.ViewHolder(convertView) {


        init {

            convertView.setOnClickListener(View.OnClickListener {

                val a = Intent(context, AdditionalActivity::class.java)
                context.startActivity(a)

            })

        }
    }


    init {

        inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getItemCount(): Int {

        return 3

    }
}