package com.cs.drcafe.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.cs.drcafe.Constants
import com.cs.drcafe.Models.DashboardList
import com.cs.drcafe.R

class BannersAdapter(var context: FragmentActivity?, var bannerlist: ArrayList<DashboardList.Banners>): PagerAdapter() {

    override fun isViewFromObject(v: View, `object`: Any): Boolean {
        // Return the current view
        return v === `object` as View
    }


    override fun getCount(): Int {
        // Count the items and return it
        return bannerlist.size
    }


    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        // Get the view from pager page layout
        val view = LayoutInflater.from(container?.context)
            .inflate(R.layout.pager_layout, container, false)

        // Get the widgets reference from layout
        var img_banner: ImageView = view.findViewById(R.id.img_banners)


        Glide.with(context!!)
            .load(Constants.IMAGE_URL + bannerlist[position].bannerImageEn)
            .into(img_banner)

        // Add the view to the parent
        container?.addView(view)

        // Return the view
        return view
    }


    override fun destroyItem(parent: ViewGroup, position: Int, `object`: Any) {
        // Remove the view from view group specified position
        parent.removeView(`object` as View)
    }


}