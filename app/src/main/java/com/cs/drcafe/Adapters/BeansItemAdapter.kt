package com.cs.drcafe.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.R

class BeansItemAdapter(
    var context: Context,
    item_list: ArrayList<String> = ArrayList()
) : RecyclerView.Adapter<BeansItemAdapter.MyViewHolder?>() {
    var inflater: LayoutInflater
    var item_list: ArrayList<String> = ArrayList()
    var pos: Int = 0
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View
//        if (language.equals("En", ignoreCase = true)) {
        itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.beans_items_list, parent, false)
//        } else {
//            itemView = LayoutInflater.from(parent.context)
//                .inflate(R.layout.driver_list_arabic, parent, false)
//        }
        //     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(
        @NonNull holder: MyViewHolder, position: Int
    ) {

        holder.item_name.setText("" + item_list[position])

        holder.main_layout.setOnClickListener {

            if (holder.bean_add_layout.visibility == View.VISIBLE) {

                holder.bean_add_layout.visibility = View.GONE
                holder.arrow.setImageDrawable(context.resources.getDrawable(R.drawable.black_down_arrow))

            } else {

                holder.bean_add_layout.visibility = View.VISIBLE
                holder.arrow.setImageDrawable(context.resources.getDrawable(R.drawable.right_arrow))

            }

        }


    }

    inner class MyViewHolder(convertView: View) :
        RecyclerView.ViewHolder(convertView) {

        var item_name: TextView
        var bean_add_layout: LinearLayout
        var main_layout: LinearLayout
        var arrow: ImageView

        init {

            item_name = convertView.findViewById(R.id.item_name)
            bean_add_layout = convertView.findViewById(R.id.bean_add_layout)
            main_layout = convertView.findViewById(R.id.main_layout)
            arrow = convertView.findViewById(R.id.arrow)

        }
    }


    init {

        this.item_list = item_list
        inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getItemCount(): Int {

        return item_list.size

    }
}