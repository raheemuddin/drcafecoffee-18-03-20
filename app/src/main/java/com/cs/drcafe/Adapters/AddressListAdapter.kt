package com.cs.drcafe.Adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.cs.bcvendor.Rest.APIInterface
import com.cs.bcvendor.Rest.ApiClient
import com.cs.drcafe.Activities.AddAddressActivity
import com.cs.drcafe.Constants
import com.cs.drcafe.Models.AddressList
import com.cs.drcafe.Models.DeleteAddressList
import com.cs.drcafe.R
import com.cs.drcafe.Widgets.NetworkUtil
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddressListAdapter(
    var context: Context,
    data: ArrayList<AddressList.dateEntry>,
    var activity: Activity?
) : RecyclerView.Adapter<AddressListAdapter.MyViewHolder?>() {
    var inflater: LayoutInflater
    lateinit var addressList: ArrayList<AddressList.dateEntry>
    var pos : Int = 0

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View
//        if (language.equals("En", ignoreCase = true)) {
        itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.address_list, parent, false)
//        } else {
//            itemView = LayoutInflater.from(parent.context)
//                .inflate(R.layout.driver_list_arabic, parent, false)
//        }
        //     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(
        @NonNull holder: MyViewHolder, position: Int
    ) {

        holder.txt_address.text = "" + addressList[position].Address
        holder.txt_address_type.text = "" + addressList[position].AddressType

        if (addressList[position].Default == 1) {
            holder.txt_default.text = "Default"
        } else {
            holder.txt_default.text = ""
        }

        holder.img_delete_address.setOnClickListener {

            pos = position

            val networkStatus: String? =
                NetworkUtil.getConnectivityStatusString(activity)
            if (!networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
                DeleteAddressListApi().execute()
            } else {
//                        if (language.equals("En", ignoreCase = true)) {
                Toast.makeText(
                    context,
                    R.string.str_connection_error,
                    Toast.LENGTH_SHORT
                ).show()
//                        } else {
//                            Toast.makeText(
//                                getApplicationContext(),
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
            }

        }

        holder.img_edit.setOnClickListener {

            var a = Intent(context, AddAddressActivity::class.java)
            a.putExtra("addresslist", addressList)
            a.putExtra("pos", position)
            a.putExtra("edit", true)
            context.startActivity(a)

        }


    }

    inner class MyViewHolder(convertView: View) :
        RecyclerView.ViewHolder(convertView) {

        var txt_address: TextView
        var txt_address_type: TextView
        var txt_default: TextView
        var img_edit: ImageView
        var img_delete_address: ImageView

        init {

            txt_address = convertView.findViewById<View>(R.id.txt_address) as TextView
            txt_address_type = convertView.findViewById<View>(R.id.txt_address_type) as TextView
            txt_default = convertView.findViewById<View>(R.id.txt_default) as TextView

            img_edit = convertView.findViewById<View>(R.id.img_edit) as ImageView
            img_delete_address =
                convertView.findViewById<View>(R.id.img_delete_address) as ImageView



            convertView.setOnClickListener(View.OnClickListener {

                //                pos = adapterPosition
                notifyDataSetChanged()

            })

            //            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, OrderTypeActivity.class);
//                    context.startActivity(a);
//
//                }
//            });
        }
    }


    init {
        this.activity = activity
        this.addressList = data
        inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getItemCount(): Int {

        return addressList.size

    }

    private fun prepareDeleteAddressJson(): String {


        val mainObj = JSONObject()
        try {

            mainObj.put("Id", addressList[pos].Id)
            mainObj.put("UserId", "14")


        } catch (je: JSONException) {
            je.printStackTrace()
        }
        Log.i("TAG", "prepareAddressListJson: $mainObj")
        return mainObj.toString()
    }

    private inner class DeleteAddressListApi :
        AsyncTask<String?, Int?, String?>() {
        //        ACProgressFlower dialog;
        var customDialog: AlertDialog? = null
        var inputStr: String? = null
        override fun onPreExecute() {
            super.onPreExecute()
            inputStr = prepareDeleteAddressJson()
            //            dialog = new ACProgressFlower.Builder(DriverListActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(DriverListActivity.this);
            val dialogBuilder =
                context?.let { AlertDialog.Builder(it) }
            // ...Irrelevant code for customizing the buttons and title
            val inflater = activity!!.layoutInflater
            val layout: Int = R.layout.progress_bar_alert
            val dialogView = inflater.inflate(layout, null)
            dialogBuilder!!.setView(dialogView)
            dialogBuilder.setCancelable(false)
            customDialog = dialogBuilder.create()
            customDialog!!.show()
            val lp = WindowManager.LayoutParams()
            val window = customDialog!!.getWindow()
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            lp.copyFrom(window.attributes)
            //This makes the progressDialog take up the full width
            val display = activity!!.windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            val screenWidth = size.x
            val d = screenWidth * 0.45
            lp.width = d.toInt()
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            window.attributes = lp
        }

        protected override fun doInBackground(vararg params: String?): String? {
            val apiService: APIInterface = ApiClient.client!!.create(APIInterface::class.java)
            val call: Call<DeleteAddressList?>? = apiService.getDeleteAddress(
                RequestBody.create(MediaType.parse("application/json"), inputStr)
            )
            call!!.enqueue(object : Callback<DeleteAddressList?> {
                override fun onResponse(
                    call: Call<DeleteAddressList?>,
                    response: Response<DeleteAddressList?>
                ) {
                    if (response.isSuccessful()) {
                        val deleteaddresslist: DeleteAddressList? = response.body()
                        if (deleteaddresslist!!.getStatus()) {


                            Log.i("TAG", "onResponse: " + deleteaddresslist.getMessageEn())

                        } else {
                            val failureResponse: String? = deleteaddresslist.getMessageEn()
//                            if (language.equals("En", ignoreCase = true)) {
                            activity?.let {
                                Constants.showOneButtonAlertDialog(
                                    failureResponse,
                                    context!!.resources.getString(R.string.app_name),
                                    context!!.resources.getString(R.string.ok),
                                    it
                                )
                            }
//                            } else {
//                                Constants.showOneButtonAlertDialog(
//                                    changePasswordResponse.messageAr,
//                                    context!!.resources.getString(R.string.app_name_ar),
//                                    context!!.resources.getString(R.string.ok_ar),
//                                    this@DriverListActivity
//                                )
//                            }
                        }
                    } else {
                        Log.i("TAG", "onrespon: ")
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.cannot_reach_server,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.cannot_reach_server_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    }
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                }

                override fun onFailure(
                    call: Call<DeleteAddressList?>,
                    t: Throwable
                ) {
                    val networkStatus: String? =
                        NetworkUtil.getConnectivityStatusString(context)
                    if (networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.str_connection_error,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    } else {
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.cannot_reach_server,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.cannot_reach_server_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    }
                    Log.i("TAG", "onFailure: " + t)
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                }
            })
            return null
        }
    }

}