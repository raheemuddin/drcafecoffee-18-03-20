package com.cs.drcafe.Adapters

import android.content.Context
import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.R

class StoreFeatureAdapter(var context: Context, images : ArrayList<Int> = ArrayList(), feature_names : ArrayList<String> = ArrayList()) :
    RecyclerView.Adapter<StoreFeatureAdapter.MyViewHolder?>() {

    var inflater: LayoutInflater
    var image: ArrayList<Int> = ArrayList()
    var feature_name : ArrayList<String> = ArrayList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View
//        if (language.equals("En", ignoreCase = true)) {
        itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.store_features_list, parent, false)
//        } else {
//            itemView = LayoutInflater.from(parent.context)
//                .inflate(R.layout.driver_list_arabic, parent, false)
//        }
        //     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(
        @NonNull holder: MyViewHolder, position: Int
    ) {

        holder.feature_txt.setText("" + feature_name[position])
        holder.feature_img.setImageDrawable(context.resources.getDrawable(image[position]))

    }

    inner class MyViewHolder(convertView: View) :
        RecyclerView.ViewHolder(convertView) {

        var feature_img : ImageView
        var feature_txt : TextView

        init {

            feature_img = convertView.findViewById(R.id.feature_img) as ImageView
            feature_txt = convertView.findViewById(R.id.feature_name) as TextView

            convertView.setOnClickListener(View.OnClickListener {

                //                pos = adapterPosition
                notifyDataSetChanged()

            })

            //            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, OrderTypeActivity.class);
//                    context.startActivity(a);
//
//                }
//            });
        }
    }


    init {
        inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        this.image = images
        this.feature_name = feature_names
    }

    override fun getItemCount(): Int {

        return feature_name.size

    }
}