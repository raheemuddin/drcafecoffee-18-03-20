package com.cs.drcafe.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Activities.MenuActivity
import com.cs.drcafe.Models.StoreInfoList
import com.cs.drcafe.R

class StoreListAdapter(
    var context: Context,
    store_list: java.util.ArrayList<StoreInfoList.dataEntry?>? = ArrayList()

) : RecyclerView.Adapter<StoreListAdapter.MyViewHolder?>() {
    var inflater: LayoutInflater
    var pos: Int = 0
    var store_list: ArrayList<StoreInfoList.dataEntry?>? = ArrayList()


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View
//        if (language.equals("En", ignoreCase = true)) {
        itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.store_map_list, parent, false)
//        } else {
//            itemView = LayoutInflater.from(parent.context)
//                .inflate(R.layout.driver_list_arabic, parent, false)
//        }
        //     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(
        @NonNull holder: MyViewHolder, position: Int
    ) {

        holder.mstore_name.setText("" + store_list!![position]!!.nameEn)
        holder.mstore_address.setText("" + store_list!![position]!!.addressEn)

    }

    inner class MyViewHolder(convertView: View) :
        RecyclerView.ViewHolder(convertView) {

        var mstore_name: TextView
        var mstore_address: TextView

        init {

            mstore_name = convertView.findViewById(R.id.store_name)
            mstore_address = convertView.findViewById(R.id.store_address)

            convertView.setOnClickListener(View.OnClickListener {

                val a = Intent(context, MenuActivity::class.java)
                a.putExtra("address", store_list!![adapterPosition]!!.addressEn)
                a.putExtra("store_id", store_list!![adapterPosition]!!.id)
                context.startActivity(a)

                //                val a = Intent(context, AdditionalActivity::class.java)
//                context.startActivity(a)

            })

        }
    }


    init {
        this.store_list = store_list
        inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getItemCount(): Int {

        return store_list!!.size

    }
}