package com.cs.drcafe.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Activities.FoodAdditionalActivity
import com.cs.drcafe.R
import java.util.*
import kotlin.collections.ArrayList

class RecommendedAdapter(
    var context: Context,
    mqty: ArrayList<Int> = ArrayList()
) : RecyclerView.Adapter<RecommendedAdapter.MyViewHolder?>() {
    var inflater: LayoutInflater
    var pos: Int = 0
    var qty: Int = 0
    var mqty: ArrayList<Int> = ArrayList()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View
//        if (language.equals("En", ignoreCase = true)) {
        itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_individual_new, parent, false)
//        } else {
//            itemView = LayoutInflater.from(parent.context)
//                .inflate(R.layout.driver_list_arabic, parent, false)
//        }
        //     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(
        @NonNull holder: MyViewHolder, position: Int
    ) {

        holder.add_bag.setOnClickListener {

            holder.add_layout.visibility = View.VISIBLE
            holder.add_bag.visibility = View.GONE

        }

        holder.qty.setText("" + mqty[position])

        holder.plus.setOnClickListener {

            mqty[position] = mqty[position] + 1;

            holder.qty.setText("" + mqty[position])

            val intent = Intent("recommended_added")
            // You can also include some extra data.
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)

        }

        holder.mins.setOnClickListener {

            if (mqty[position] >= 1) {
                mqty[position] = mqty[position] - 1;

                val intent = Intent("recommended_added")
                // You can also include some extra data.
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent)

            }

            holder.qty.setText("" + mqty[position])


            if (holder.qty.text.toString().equals("0")) {

                holder.add_layout.visibility = View.GONE
                holder.add_bag.visibility = View.VISIBLE

            }

        }

    }

    inner class MyViewHolder(convertView: View) :
        RecyclerView.ViewHolder(convertView) {

        var add_bag: LinearLayout
        var add_layout: LinearLayout
        var cal_layout: LinearLayout
        var plus: ImageView
        var qty: TextView
        var mins: ImageView

        init {

            add_bag = convertView.findViewById(R.id.add_bag) as LinearLayout
            add_layout = convertView.findViewById(R.id.add_layout) as LinearLayout
            cal_layout = convertView.findViewById(R.id.cal_layout) as LinearLayout

            plus = convertView.findViewById(R.id.plus) as ImageView
            mins = convertView.findViewById(R.id.mins) as ImageView

            qty = convertView.findViewById(R.id.qty) as TextView

            convertView.setOnClickListener(View.OnClickListener {

                //                pos = adapterPosition
//                if (food_boolean) {
//                    var a = Intent(context, FoodAdditionalActivity::class.java)
//                    context.startActivity(a)
//                }
                notifyDataSetChanged()

            })

            //            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, OrderTypeActivity.class);
//                    context.startActivity(a);
//
//                }
//            });
        }
    }


    init {

        inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        this.mqty = mqty

    }

    override fun getItemCount(): Int {

        return 2

    }
}