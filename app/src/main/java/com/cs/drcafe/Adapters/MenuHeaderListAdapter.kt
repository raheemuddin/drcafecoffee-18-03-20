package com.cs.drcafe.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Activities.ItemsindividualActivity.Companion.str_menu_name
import com.cs.drcafe.Activities.ItemsindividualActivity.Companion.str_menu_name1
import com.cs.drcafe.R
import java.util.*

class MenuHeaderListAdapter(
    var context: Context,
    orderItems: ArrayList<String>
) : RecyclerView.Adapter<MenuHeaderListAdapter.MyViewHolder?>() {
    var inflater: LayoutInflater
    var orderItems: ArrayList<String> = ArrayList<String>()
    var cardViewList: List<TextView> = ArrayList()
    var pos: Int = 0
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View
//        if (language.equals("En", ignoreCase = true)) {
        itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.menu_header, parent, false)
//        } else {
//            itemView = LayoutInflater.from(parent.context)
//                .inflate(R.layout.driver_list_arabic, parent, false)
//        }
        //     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(
        @NonNull holder: MyViewHolder, position: Int
    ) {
        if (str_menu_name1.equals(orderItems[position])) {

            holder.header_name.background =
                context.resources.getDrawable(R.drawable.menu_selected_bg)

        } else {

            holder.header_name.setBackgroundColor(context.resources.getColor(R.color.white))

        }

        holder.header_name.setOnClickListener {

            val intent = Intent("ChangeAdapter")
            // You can also include some extra data.
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
            pos = position
            str_menu_name1 = orderItems[pos]
            notifyDataSetChanged()

        }

        holder.header_name.text = "" + orderItems[position]

    }

    inner class MyViewHolder(convertView: View) :
        RecyclerView.ViewHolder(convertView) {
        //        var rating: TextView
        var header_name: TextView
        //        var assign_cancel: TextView
//        var live_tracking: TextView
//        var driver_pic: CircleImageView
        var main_layout: LinearLayout

        init {

            header_name = convertView.findViewById<View>(R.id.header_name) as TextView
            main_layout =
                convertView.findViewById<View>(R.id.main_layout) as LinearLayout

            convertView.setOnClickListener(View.OnClickListener {

                //                pos = adapterPosition
                notifyDataSetChanged()

            })

            //            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, OrderTypeActivity.class);
//                    context.startActivity(a);
//
//                }
//            });
        }
    }


    init {
        this.orderItems = orderItems
        inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getItemCount(): Int {

        return orderItems.size

    }
}