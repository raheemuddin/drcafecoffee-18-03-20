package com.cs.drcafe.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Activities.ItemsindividualActivity.Companion.headerListAdapter
import com.cs.drcafe.Activities.ItemsindividualActivity.Companion.str_menu_name1
import com.cs.drcafe.R

class BeansMenuSubItemListAdapter(
    var context: Context,
    sub_item_list: ArrayList<String> = ArrayList(),
    item_list: ArrayList<String> = ArrayList()
) : RecyclerView.Adapter<BeansMenuSubItemListAdapter.MyViewHolder?>() {
    var inflater: LayoutInflater
    var pos: Int = 0
    var sub_item_list: ArrayList<String> = ArrayList()
    var item_list: ArrayList<String> = ArrayList()
    lateinit var madapter: BeansMenuItemListAdapter
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View
//        if (language.equals("En", ignoreCase = true)) {
        itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.beans_menu_group, parent, false)
//        } else {
//            itemView = LayoutInflater.from(parent.context)
//                .inflate(R.layout.driver_list_arabic, parent, false)
//        }
        //     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(
        @NonNull holder: MyViewHolder, position: Int
    ) {

        holder.beans_sub_item_name.text = sub_item_list[position]

        if (sub_item_list[position].equals(str_menu_name1)) {

            holder.beans_list.visibility = View.VISIBLE
            holder.arrow.setImageDrawable(context.resources.getDrawable(R.drawable.right_arrow))

        } else {

            holder.beans_list.visibility = View.GONE
            holder.arrow.setImageDrawable(context.resources.getDrawable(R.drawable.black_down_arrow))

        }

        holder.main_layout.setOnClickListener {

            if (holder.beans_list.visibility == View.VISIBLE) {

                holder.beans_list.visibility = View.GONE
                holder.arrow.setImageDrawable(context.resources.getDrawable(R.drawable.black_down_arrow))

            } else {

                holder.beans_list.visibility = View.VISIBLE
                holder.arrow.setImageDrawable(context.resources.getDrawable(R.drawable.right_arrow))

            }
            str_menu_name1 = sub_item_list[position]
            headerListAdapter.notifyDataSetChanged()

            notifyDataSetChanged()

        }

        val linearLayoutManager1 = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        holder.beans_list!!.setLayoutManager(linearLayoutManager1)
        madapter = BeansMenuItemListAdapter(
            context, item_list
        )
        holder.beans_list!!.setAdapter(madapter)

        if (position == 0) {

            holder.img.setImageDrawable(context.resources.getDrawable(R.drawable.bean_img))

        } else if (position == 1) {

            holder.img.setImageDrawable(context.resources.getDrawable(R.drawable.bean_img1))

        } else if (position == 2) {

            holder.img.setImageDrawable(context.resources.getDrawable(R.drawable.bean_img2))

        } else if (position == 3) {

            holder.img.setImageDrawable(context.resources.getDrawable(R.drawable.bean_img3))

        }

    }

    inner class MyViewHolder(convertView: View) :
        RecyclerView.ViewHolder(convertView) {

        var main_layout: RelativeLayout
        var beans_sub_item_name: TextView
        var beans_list: RecyclerView
        var arrow: ImageView
        var img: ImageView


        init {

            main_layout = convertView.findViewById(R.id.main_layout) as RelativeLayout
            beans_sub_item_name = convertView.findViewById(R.id.beans_sub_item_name) as TextView
            beans_list = convertView.findViewById(R.id.beans_list) as RecyclerView
            arrow = convertView.findViewById(R.id.arrow) as ImageView
            img = convertView.findViewById(R.id.img) as ImageView


            convertView.setOnClickListener(View.OnClickListener {

                //                pos = adapterPosition
                notifyDataSetChanged()

            })

            //            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, OrderTypeActivity.class);
//                    context.startActivity(a);
//
//                }
//            });
        }
    }


    init {

        this.sub_item_list = sub_item_list
        this.item_list = item_list
        inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getItemCount(): Int {

        return sub_item_list.size

    }
}