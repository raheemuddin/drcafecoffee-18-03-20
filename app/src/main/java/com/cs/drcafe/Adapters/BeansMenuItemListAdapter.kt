package com.cs.drcafe.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Activities.BeansItemActivity
import com.cs.drcafe.R

class BeansMenuItemListAdapter(
    var context: Context,
    item_list: ArrayList<String> = ArrayList()
) : RecyclerView.Adapter<BeansMenuItemListAdapter.MyViewHolder?>() {
    var inflater: LayoutInflater
    var item_list: ArrayList<String> = ArrayList()
    var pos: Int = 0
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val itemView: View
//        if (language.equals("En", ignoreCase = true)) {
        itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.beans_menu_item, parent, false)
//        } else {
//            itemView = LayoutInflater.from(parent.context)
//                .inflate(R.layout.driver_list_arabic, parent, false)
//        }
        //     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(
        @NonNull holder: MyViewHolder, position: Int
    ) {

        holder.item_name.setText("" + item_list[position])

        holder.main_layout.setOnClickListener {

            var a = Intent(context, BeansItemActivity::class.java)
            context.startActivity(a)

        }


    }

    inner class MyViewHolder(convertView: View) :
        RecyclerView.ViewHolder(convertView) {

        var item_name: TextView
        var main_layout: LinearLayout

        init {

            item_name = convertView.findViewById(R.id.item_name)
            main_layout = convertView.findViewById(R.id.main_layout)

        }
    }


    init {

        this.item_list = item_list
        inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getItemCount(): Int {

        return item_list.size

    }
}