package com.cs.drcafe.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

class StoreInfoList {

    @Expose
    @SerializedName("Data")
    private var Data: ArrayList<dataEntry?>? = null

    @Expose
    @SerializedName("MessageAr")
    private var MessageAr: String? = null

    @Expose
    @SerializedName("MessageEn")
    private var MessageEn: String? = null

    @Expose
    @SerializedName("Status")
    private var Status = false

    fun getData(): ArrayList<dataEntry?>? {
        return Data
    }

    fun setData(Data: ArrayList<dataEntry?>?) {
        this.Data = Data
    }

    fun getMessageAr(): String? {
        return MessageAr
    }

    fun setMessageAr(MessageAr: String?) {
        this.MessageAr = MessageAr
    }

    fun getMessageEn(): String? {
        return MessageEn
    }

    fun setMessageEn(MessageEn: String?) {
        this.MessageEn = MessageEn
    }

    fun getStatus(): Boolean {
        return Status
    }

    fun setStatus(Status: Boolean) {
        this.Status = Status
    }

    class dataEntry : Serializable {
        @Expose
        @SerializedName("Segments")
        var segments: ArrayList<Segments>? = null

        @Expose
        @SerializedName("Facilities")
        var facilities: ArrayList<Facilities>? = null

        @Expose
        @SerializedName("CountryNameAr")
        var countryNameAr: String? = null

        @Expose
        @SerializedName("CountryNameEn")
        var countryNameEn: String? = null

        @Expose
        @SerializedName("CountryId")
        var countryId = 0

        @Expose
        @SerializedName("FreeDrinkLimit")
        var freeDrinkLimit: String? = null

        @Expose
        @SerializedName("StoreLounchDate")
        var storeLounchDate: String? = null

        @Expose
        @SerializedName("GroupNameAr")
        var groupNameAr: String? = null

        @Expose
        @SerializedName("GroupNameEn")
        var groupNameEn: String? = null

        @Expose
        @SerializedName("GroupId")
        var groupId = 0

        @Expose
        @SerializedName("Favourite")
        var favourite = false

        @Expose
        @SerializedName("ShiftId")
        var shiftId = 0

        @Expose
        @SerializedName("ImagePath")
        var imagePath: String? = null

        @Expose
        @SerializedName("IsActive")
        var isActive = false

        @Expose
        @SerializedName("IsTrending")
        var isTrending = false

        @Expose
        @SerializedName("StoreStatus")
        var storeStatus = false

        @Expose
        @SerializedName("ExclusiveForWomen")
        var exclusiveForWomen = false

        @Expose
        @SerializedName("AddressAr")
        var addressAr: String? = null

        @Expose
        @SerializedName("AddressEn")
        var addressEn: String? = null

        @Expose
        @SerializedName("MobileNo")
        var mobileNo: String? = null

        @Expose
        @SerializedName("Distance")
        var distance: String? = null

        @Expose
        @SerializedName("Longitude")
        var longitude: String? = null

        @Expose
        @SerializedName("Latitude")
        var latitude: String? = null

        @Expose
        @SerializedName("LandlineNo")
        var landlineNo: String? = null

        @Expose
        @SerializedName("DescAr")
        var descAr: String? = null

        @Expose
        @SerializedName("DescEn")
        var descEn: String? = null

        @Expose
        @SerializedName("NameAr")
        var nameAr: String? = null

        @Expose
        @SerializedName("NameEn")
        var nameEn: String? = null

        @Expose
        @SerializedName("StoreCode")
        var storeCode: String? = null

        @Expose
        @SerializedName("Id")
        var id = 0
    }

    class Segments : Serializable {
        @Expose
        @SerializedName("NameAr")
        var nameAr: String? = null

        @Expose
        @SerializedName("NameEn")
        var nameEn: String? = null

        @Expose
        @SerializedName("Id")
        var id = 0
    }

    class Facilities : Serializable {
        @Expose
        @SerializedName("Image")
        var image: String? = null

        @Expose
        @SerializedName("NameAr")
        var nameAr: String? = null

        @Expose
        @SerializedName("NameEn")
        var nameEn: String? = null

        @Expose
        @SerializedName("Id")
        var id = 0
    }

}