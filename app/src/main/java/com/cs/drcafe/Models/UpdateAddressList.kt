package com.cs.drcafe.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateAddressList {

    @Expose
    @SerializedName("Data")
    private var Data: dateEntry? = null

    @Expose
    @SerializedName("MessageAr")
    private var MessageAr: String? = null

    @Expose
    @SerializedName("MessageEn")
    private var MessageEn: String? = null

    @Expose
    @SerializedName("Status")
    private var Status = false

    fun getData(): dateEntry? {
        return Data
    }

    fun setData(Data: dateEntry?) {
        this.Data = Data
    }

    fun getMessageAr(): String? {
        return MessageAr
    }

    fun setMessageAr(MessageAr: String?) {
        this.MessageAr = MessageAr
    }

    fun getMessageEn(): String? {
        return MessageEn
    }

    fun setMessageEn(MessageEn: String?) {
        this.MessageEn = MessageEn
    }

    fun getStatus(): Boolean {
        return Status
    }

    fun setStatus(Status: Boolean) {
        this.Status = Status
    }

    class dateEntry {
        @Expose
        @SerializedName("StatusMessage")
        var statusMessage: String? = null

        @Expose
        @SerializedName("StatusCode")
        var statusCode: String? = null

        @Expose
        @SerializedName("CreatedDate")
        var createdDate: String? = null

        @Expose
        @SerializedName("isActive")
        var isActive = false

        @Expose
        @SerializedName("ContactNo")
        var contactNo: String? = null

        @Expose
        @SerializedName("ContactPerson")
        var contactPerson: String? = null

        @Expose
        @SerializedName("Longitude")
        var longitude: String? = null

        @Expose
        @SerializedName("Latitude")
        var latitude: String? = null

        @Expose
        @SerializedName("Default")
        var default = 0

        @Expose
        @SerializedName("CountryCode")
        var countryCode: String? = null

        @Expose
        @SerializedName("AddressType")
        var addressType: String? = null

        @Expose
        @SerializedName("Address")
        var address: String? = null

        @Expose
        @SerializedName("LandMark")
        var landMark: String? = null

        @Expose
        @SerializedName("HouseName")
        var houseName: String? = null

        @Expose
        @SerializedName("HouseNo")
        var houseNo: String? = null

        @Expose
        @SerializedName("UserId")
        var userId = 0

        @Expose
        @SerializedName("Id")
        var id = 0
    }

}