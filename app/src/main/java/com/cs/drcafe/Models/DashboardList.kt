package com.cs.drcafe.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DashboardList {

    @Expose
    @SerializedName("Data")
    private var Data: dataEntry? = null

    @Expose
    @SerializedName("MessageAr")
    private var MessageAr: String? = null

    @Expose
    @SerializedName("MessageEn")
    private var MessageEn: String? = null

    @Expose
    @SerializedName("Status")
    private var Status = false

    fun getData(): dataEntry? {
        return Data
    }

    fun setData(Data: dataEntry?) {
        this.Data = Data
    }

    fun getMessageAr(): String? {
        return MessageAr
    }

    fun setMessageAr(MessageAr: String?) {
        this.MessageAr = MessageAr
    }

    fun getMessageEn(): String? {
        return MessageEn
    }

    fun setMessageEn(MessageEn: String?) {
        this.MessageEn = MessageEn
    }

    fun getStatus(): Boolean {
        return Status
    }

    fun setStatus(Status: Boolean) {
        this.Status = Status
    }

    class dataEntry {
        @Expose
        @SerializedName("Programs")
        var programs: Programs? = null

        @Expose
        @SerializedName("OrderTypes")
        var orderTypes: OrderTypes? = null

        @Expose
        @SerializedName("themes")
        var themes: ArrayList<Themes>? = null

        @Expose
        @SerializedName("updateInfo")
        var updateInfo: ArrayList<UpdateInfo>? = null

        @Expose
        @SerializedName("Orders")
        var orders: ArrayList<String>? = null

        @Expose
        @SerializedName("PopUps")
        var popUps: ArrayList<PopUps>? = null

        @Expose
        @SerializedName("Banners")
        var banners: ArrayList<Banners>? = null
    }

    class Programs {
        @Expose
        @SerializedName("Gift")
        var gift: Gift? = null

        @Expose
        @SerializedName("Catering")
        var catering: Catering? = null

        @Expose
        @SerializedName("Subscription")
        var subscription: Subscription? = null
    }

    class Gift {
        @Expose
        @SerializedName("MessageAr")
        var messageAr: String? = null

        @Expose
        @SerializedName("MessageEn")
        var messageEn: String? = null

        @Expose
        @SerializedName("Status")
        var status = false
    }

    class Catering {
        @Expose
        @SerializedName("MessageAr")
        var messageAr: String? = null

        @Expose
        @SerializedName("MessageEn")
        var messageEn: String? = null

        @Expose
        @SerializedName("Status")
        var status = false
    }

    class Subscription {
        @Expose
        @SerializedName("MessageAr")
        var messageAr: String? = null

        @Expose
        @SerializedName("MessageEn")
        var messageEn: String? = null

        @Expose
        @SerializedName("Status")
        var status = false
    }

    class OrderTypes {
        @Expose
        @SerializedName("Delivery")
        var delivery: Delivery? = null

        @Expose
        @SerializedName("PickUp")
        var pickUp: PickUp? = null

        @Expose
        @SerializedName("DineIn")
        var dineIn: DineIn? = null
    }

    class Delivery {
        @Expose
        @SerializedName("MessageAr")
        var messageAr: String? = null

        @Expose
        @SerializedName("MessageEn")
        var messageEn: String? = null

        @Expose
        @SerializedName("Status")
        var status = false
    }

    class PickUp {
        @Expose
        @SerializedName("MessageAr")
        var messageAr: String? = null

        @Expose
        @SerializedName("MessageEn")
        var messageEn: String? = null

        @Expose
        @SerializedName("Status")
        var status = false
    }

    class DineIn {
        @Expose
        @SerializedName("MessageAr")
        var messageAr: String? = null

        @Expose
        @SerializedName("MessageEn")
        var messageEn: String? = null

        @Expose
        @SerializedName("Status")
        var status = false
    }

    class Themes: Serializable {
        @Expose
        @SerializedName("IsSelected")
        var isSelected = false

        @Expose
        @SerializedName("DefaultSelection")
        var defaultSelection = false

        @Expose
        @SerializedName("ForegroundColor")
        var foregroundColor: String? = null

        @Expose
        @SerializedName("BackgroundColor")
        var backgroundColor: String? = null

        @Expose
        @SerializedName("Id")
        var id = 0
    }

    class UpdateInfo: Serializable {
        @Expose
        @SerializedName("UpdateSeverity")
        var updateSeverity = 0

        @Expose
        @SerializedName("UpdatesAvailable")
        var updatesAvailable = 0

        @Expose
        @SerializedName("AppVersion")
        var appVersion: String? = null
    }

    class PopUps: Serializable {
        @Expose
        @SerializedName("WebUrl")
        var webUrl: String? = null

        @Expose
        @SerializedName("SubscriptionId")
        var subscriptionId = 0

        @Expose
        @SerializedName("PackageId")
        var packageId = 0

        @Expose
        @SerializedName("OfferId")
        var offerId = 0

        @Expose
        @SerializedName("ItemId")
        var itemId = 0

        @Expose
        @SerializedName("SubCategoryId")
        var subCategoryId = 0

        @Expose
        @SerializedName("CategoryId")
        var categoryId = 0

        @Expose
        @SerializedName("MainCategoryId")
        var mainCategoryId = 0

        @Expose
        @SerializedName("ImageAr")
        var imageAr: String? = null

        @Expose
        @SerializedName("ImageEn")
        var imageEn: String? = null

        @Expose
        @SerializedName("TypeOfPopUp")
        var typeOfPopUp: String? = null

        @Expose
        @SerializedName("Name")
        var name: String? = null

        @Expose
        @SerializedName("Id")
        var id = 0
    }

    class Banners: Serializable {
        @Expose
        @SerializedName("SequenceId")
        var sequenceId = 0

        @Expose
        @SerializedName("ItemId")
        var itemId = 0

        @Expose
        @SerializedName("SubCategoryId")
        var subCategoryId = 0

        @Expose
        @SerializedName("CategoryId")
        var categoryId = 0

        @Expose
        @SerializedName("MainCategoryId")
        var mainCategoryId = 0

        @Expose
        @SerializedName("IsClick")
        var isClick = false

        @Expose
        @SerializedName("BannerImageAr")
        var bannerImageAr: String? = null

        @Expose
        @SerializedName("BannerImageEn")
        var bannerImageEn: String? = null

        @Expose
        @SerializedName("BannerName")
        var bannerName: String? = null

        @Expose
        @SerializedName("Id")
        var id = 0
    }

}