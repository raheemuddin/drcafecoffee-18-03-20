package com.cs.drcafe.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AddressList {

    @Expose
    @SerializedName("Data")
    private var Data: ArrayList<dateEntry>? = null

    @Expose
    @SerializedName("MessageAr")
    private var MessageAr: String? = null

    @Expose
    @SerializedName("MessageEn")
    private var MessageEn: String? = null

    @Expose
    @SerializedName("Status")
    private var Status = false

    fun getData(): ArrayList<dateEntry>? {
        return Data
    }

    fun setData(Data: ArrayList<dateEntry>?) {
        this.Data = Data
    }

    fun getMessageAr(): String? {
        return MessageAr
    }

    fun setMessageAr(MessageAr: String?) {
        this.MessageAr = MessageAr
    }

    fun getMessageEn(): String? {
        return MessageEn
    }

    fun setMessageEn(MessageEn: String?) {
        this.MessageEn = MessageEn
    }

    fun getStatus(): Boolean {
        return Status
    }

    fun setStatus(Status: Boolean) {
        this.Status = Status
    }

    class dateEntry : Serializable{

        @Expose
        @SerializedName("Id")
        var Id: Int = 0

        @Expose
        @SerializedName("UserId")
        var UserId: Int = 0

        @Expose
        @SerializedName("HouseNo")
        var HouseNo: String = ""

        @Expose
        @SerializedName("HouseName")
        var HouseName: String = ""

        @Expose
        @SerializedName("LandMark")
        var LandMark: String = ""

        @Expose
        @SerializedName("Address")
        var Address: String = ""

        @Expose
        @SerializedName("AddressType")
        var AddressType: String = ""

        @Expose
        @SerializedName("CountryCode")
        var CountryCode: String = ""

        @Expose
        @SerializedName("Default")
        var Default: Int = 0

        @Expose
        @SerializedName("Latitude")
        var Latitude: String = ""

        @Expose
        @SerializedName("Longitude")
        var Longitude: String = ""

        @Expose
        @SerializedName("ContactPerson")
        var ContactPerson: String = ""

        @Expose
        @SerializedName("ContactNo")
        var ContactNo: String = ""

        @Expose
        @SerializedName("isActive")
        var isActive: Boolean = false

        @Expose
        @SerializedName("CreatedDate")
        var CreatedDate: String = ""

        @Expose
        @SerializedName("StatusCode")
        var StatusCode: String = ""

        @Expose
        @SerializedName("StatusMessage")
        var StatusMessage: String = ""

    }

}
