package com.cs.drcafe.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

class StoreCategoryList {

    @Expose
    @SerializedName("Data")
    private var Data: dataEntry? = null

    @Expose
    @SerializedName("MessageAr")
    private var MessageAr: String? = null

    @Expose
    @SerializedName("MessageEn")
    private var MessageEn: String? = null

    @Expose
    @SerializedName("Status")
    private var Status = false

    fun getData(): dataEntry? {
        return Data
    }

    fun setData(Data: dataEntry?) {
        this.Data = Data
    }

    fun getMessageAr(): String? {
        return MessageAr
    }

    fun setMessageAr(MessageAr: String?) {
        this.MessageAr = MessageAr
    }

    fun getMessageEn(): String? {
        return MessageEn
    }

    fun setMessageEn(MessageEn: String?) {
        this.MessageEn = MessageEn
    }

    fun getStatus(): Boolean {
        return Status
    }

    fun setStatus(Status: Boolean) {
        this.Status = Status
    }

    class dataEntry : Serializable {
        @Expose
        @SerializedName("SubCategory")
        var subCategory: ArrayList<SubCategory>? = null

        @Expose
        @SerializedName("Category")
        var category: ArrayList<Category>? = null

        @Expose
        @SerializedName("MainCategory")
        var mainCategory: ArrayList<MainCategory>? = null
    }

    class SubCategory : Serializable {
        @Expose
        @SerializedName("ParentCategoryId")
        var parentCategoryId = 0

        @Expose
        @SerializedName("Level")
        var level = 0

        @Expose
        @SerializedName("NameAr")
        var nameAr: String? = null

        @Expose
        @SerializedName("NameEn")
        var nameEn: String? = null

        @Expose
        @SerializedName("Id")
        var id = 0
    }

    class Category : Serializable {
        @Expose
        @SerializedName("ParentCategoryId")
        var parentCategoryId = 0

        @Expose
        @SerializedName("Level")
        var level = 0

        @Expose
        @SerializedName("NameAr")
        var nameAr: String? = null

        @Expose
        @SerializedName("NameEn")
        var nameEn: String? = null

        @Expose
        @SerializedName("Id")
        var id = 0
    }

    class MainCategory : Serializable {
        @Expose
        @SerializedName("ParentCategoryId")
        var parentCategoryId = 0

        @Expose
        @SerializedName("Level")
        var level = 0

        @Expose
        @SerializedName("NameAr")
        var nameAr: String? = null

        @Expose
        @SerializedName("NameEn")
        var nameEn: String? = null

        @Expose
        @SerializedName("Id")
        var id = 0
    }

}