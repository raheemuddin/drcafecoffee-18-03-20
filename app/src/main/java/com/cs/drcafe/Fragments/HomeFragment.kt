package com.cs.drcafe.Fragments

import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.cs.bcvendor.Rest.APIInterface
import com.cs.bcvendor.Rest.ApiClient
import com.cs.drcafe.Activities.*
import com.cs.drcafe.Adapters.BannersAdapter
import com.cs.drcafe.Constants
import com.cs.drcafe.Models.DashboardList
import com.cs.drcafe.R
import com.cs.drcafe.Widgets.NetworkUtil
import com.github.demono.AutoScrollViewPager
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment : Fragment() {

    lateinit var rootView: View
    lateinit var order_layout: LinearLayout
    lateinit var subcrib_layout: LinearLayout
    lateinit var catering_layout: LinearLayout
    lateinit var deal_layout: LinearLayout

    lateinit var delivery_layout: LinearLayout
    lateinit var pickup_layout: LinearLayout
    lateinit var dine_in_layout: LinearLayout
    lateinit var history_layout: LinearLayout

    lateinit var order_bg: RelativeLayout
    lateinit var subcrib_bg: RelativeLayout
    lateinit var catering_bg: RelativeLayout
    lateinit var deal_bg: RelativeLayout

    lateinit var autoscroll: AutoScrollViewPager

    var mBanners: BannersAdapter? = null

    @Nullable
    @Override
    override fun onCreateView(
        @NonNull inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {

        rootView = inflater.inflate(R.layout.home_screen, container, false);

        order_layout = rootView.findViewById(R.id.order_layout) as LinearLayout
        subcrib_layout = rootView.findViewById(R.id.subscription_layout) as LinearLayout
        catering_layout = rootView.findViewById(R.id.catering_layout) as LinearLayout
        deal_layout = rootView.findViewById(R.id.deal_layout) as LinearLayout

        delivery_layout = rootView.findViewById(R.id.delivery_layout) as LinearLayout
        pickup_layout = rootView.findViewById(R.id.pickup_layout) as LinearLayout
        dine_in_layout = rootView.findViewById(R.id.dine_in_layout) as LinearLayout
        history_layout = rootView.findViewById(R.id.history_layout) as LinearLayout

        order_bg = rootView.findViewById(R.id.order_bg) as RelativeLayout
        subcrib_bg = rootView.findViewById(R.id.subscrib_bg) as RelativeLayout
        catering_bg = rootView.findViewById(R.id.catering_bg) as RelativeLayout
        deal_bg = rootView.findViewById(R.id.deal_bg) as RelativeLayout

        autoscroll = rootView.findViewById(R.id.autoscroll) as AutoScrollViewPager

        delivery_layout.setOnClickListener {

            val a = Intent(activity, AddressActivity::class.java)
            startActivity(a)

        }

        pickup_layout.setOnClickListener {

            val a = Intent(activity, StoreDetailsActivity::class.java)
            startActivity(a)

        }

        order_layout.setOnClickListener {

            val a = Intent(activity, OrderActivity::class.java)
            startActivity(a)

            order_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle_selected))
            subcrib_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle))
            catering_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle))
            deal_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle))

        }

        subcrib_layout.setOnClickListener {

            //            val a = Intent(activity, OrderfActivity::class.java)
//            startActivity(a)

            order_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle))
            subcrib_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle_selected))
            catering_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle))
            deal_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle))

        }

        catering_layout.setOnClickListener {

            val a = Intent(activity, CateringActivity::class.java)
            startActivity(a)

            order_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle))
            subcrib_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle))
            catering_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle_selected))
            deal_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle))

        }

        deal_layout.setOnClickListener {

            val a = Intent(activity, QuickShareActivity::class.java)
            startActivity(a)

            order_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle))
            subcrib_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle))
            catering_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle))
            deal_bg.setBackgroundDrawable(resources.getDrawable(R.drawable.home_circle_selected))

        }

        val networkStatus: String? =
            NetworkUtil.getConnectivityStatusString(activity)
        if (!networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
            DashboardDetails().execute()
        } else {
//                        if (language.equals("En", ignoreCase = true)) {
            Toast.makeText(
                activity,
                R.string.str_connection_error,
                Toast.LENGTH_SHORT
            ).show()
//                        } else {
//                            Toast.makeText(
//                                getApplicationContext(),
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
        }

        return rootView
    }

    private fun prepareDashboardJson(): String {

        val mainObj = JSONObject()
        try {

            mainObj.put("UserId", "14")
            mainObj.put("AppVersion", "1.0")
            mainObj.put("AppType", "Android")
            mainObj.put("DeviceToken", "abcdefghijklmnopqrstuvwxyz")

        } catch (je: JSONException) {
            je.printStackTrace()
        }
        Log.i("TAG", "prepareDashboardJson: $mainObj")
        return mainObj.toString()
    }

    private inner class DashboardDetails :
        AsyncTask<String?, Int?, String?>() {
        //        ACProgressFlower dialog;
        var customDialog: AlertDialog? = null
        var inputStr: String? = null
        override fun onPreExecute() {
            super.onPreExecute()
            inputStr = prepareDashboardJson()
            //            dialog = new ACProgressFlower.Builder(DriverListActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(DriverListActivity.this);
            val dialogBuilder =
                context?.let { AlertDialog.Builder(it) }
            // ...Irrelevant code for customizing the buttons and title
            val inflater = layoutInflater
            val layout: Int = R.layout.progress_bar_alert
            val dialogView = inflater.inflate(layout, null)
            dialogBuilder!!.setView(dialogView)
            dialogBuilder.setCancelable(false)
            customDialog = dialogBuilder.create()
            customDialog!!.show()
            val lp = WindowManager.LayoutParams()
            val window = customDialog!!.getWindow()
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            lp.copyFrom(window.attributes)
            //This makes the progressDialog take up the full width
            val display = activity!!.windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            val screenWidth = size.x
            val d = screenWidth * 0.45
            lp.width = d.toInt()
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            window.attributes = lp
        }

        protected override fun doInBackground(vararg params: String?): String? {
            val apiService: APIInterface = ApiClient.client!!.create(APIInterface::class.java)
            val call: Call<DashboardList?>? = apiService.getDashboard(
                RequestBody.create(MediaType.parse("application/json"), inputStr)
            )
            call!!.enqueue(object : Callback<DashboardList?> {
                override fun onResponse(
                    call: Call<DashboardList?>,
                    response: Response<DashboardList?>
                ) {
                    if (response.isSuccessful()) {
                        val dashboardList: DashboardList? = response.body()
                        if (dashboardList!!.getStatus()) {

                            Log.i("TAG", "onResponse: " + dashboardList.getData()!!.banners!!.size)

                            try {
                                mBanners =
                                     BannersAdapter(getActivity(), dashboardList.getData()!!.banners!!)
                                autoscroll.setAdapter(mBanners)
                                mBanners!!.notifyDataSetChanged()

                                autoscroll.slideInterval = 3000
                                autoscroll.setCycle(true)
                                autoscroll.startAutoScroll()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }


                        } else {
                            val failureResponse: String? = dashboardList.getMessageEn()
//                            if (language.equals("En", ignoreCase = true)) {
                            Constants.showOneButtonAlertDialog(
                                failureResponse,
                                context!!.resources.getString(R.string.app_name),
                                context!!.resources.getString(R.string.ok),
                                activity!!
                            )
//                            } else {
//                                Constants.showOneButtonAlertDialog(
//                                    changePasswordResponse.messageAr,
//                                    context!!.resources.getString(R.string.app_name_ar),
//                                    context!!.resources.getString(R.string.ok_ar),
//                                    this@DriverListActivity
//                                )
//                            }
                        }
                    } else {
                        Log.i("TAG", "onrespon: ")
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            activity,
                            R.string.cannot_reach_server,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.cannot_reach_server_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    }
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                }

                override fun onFailure(
                    call: Call<DashboardList?>,
                    t: Throwable
                ) {
                    val networkStatus: String? =
                        NetworkUtil.getConnectivityStatusString(activity)
                    if (networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            activity,
                            R.string.str_connection_error,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    } else {
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            activity,
                            R.string.cannot_reach_server,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.cannot_reach_server_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    }
                    Log.i("TAG", "onFailure: " + t)
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                }
            })
            return null
        }
    }

}
