package com.cs.drcafe.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import com.cs.drcafe.Activities.StoreActivity
import com.cs.drcafe.R

class OrderFragment : Fragment() {

    lateinit var rootView: View
    lateinit var back_btn: ImageView
    lateinit var order_layout: LinearLayout

    @Nullable
    @Override
    override fun onCreateView(@NonNull inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {

        rootView = inflater.inflate(R.layout.order_screen, container, false);

        back_btn = rootView.findViewById(R.id.back_btn) as ImageView

        order_layout = rootView.findViewById(R.id.order_layout) as LinearLayout

        back_btn.visibility = View.GONE

        back_btn.setOnClickListener {
        }

        order_layout.setOnClickListener {

            val a = Intent(activity, StoreActivity::class.java)
            startActivity(a)

        }

        return rootView
    }



}