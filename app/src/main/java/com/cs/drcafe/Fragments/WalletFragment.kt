package com.cs.drcafe.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import com.cs.drcafe.Activities.DrCafeWalletActivity
import com.cs.drcafe.R

class WalletFragment : Fragment() {

    lateinit var rootView: View
    lateinit var dr_cafe_layout: LinearLayout

    @Nullable
    @Override
    override fun onCreateView(@NonNull inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {

        rootView = inflater.inflate(R.layout.activity_wallet, container, false);

        dr_cafe_layout = rootView.findViewById(R.id.dr_cafe_layout)

        dr_cafe_layout.setOnClickListener {

            val a = Intent(activity, DrCafeWalletActivity::class.java)
            startActivity(a)

        }


        return rootView
    }

}