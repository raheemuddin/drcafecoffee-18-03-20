package com.cs.drcafe.Fragments

import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.bcvendor.Rest.APIInterface
import com.cs.bcvendor.Rest.ApiClient
import com.cs.drcafe.Activities.MenuActivity
import com.cs.drcafe.Adapters.StoreListAdapter
import com.cs.drcafe.Constants
import com.cs.drcafe.Dialogs.CustomDialog
import com.cs.drcafe.Models.StoreInfoList
import com.cs.drcafe.R
import com.cs.drcafe.Widgets.NetworkUtil
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.ui.IconGenerator
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


class StoreFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    lateinit var rootView: View
    private var mMap: GoogleMap? = null
    lateinit var marker1: Marker
    lateinit var marker2: Marker
    lateinit var marker3: Marker
    lateinit var marker4: Marker
    lateinit var back_btn: ImageView
    var store_name: ArrayList<String> = ArrayList()
    var store_address: ArrayList<String> = ArrayList()
    lateinit var madapter: StoreListAdapter
    lateinit var img: ImageView
    lateinit var map_layout: RelativeLayout
    lateinit var map_list: RecyclerView

    companion object {
        var customDialogPop: CustomDialog? = null
    }

    @Nullable
    @Override
    override fun onCreateView(
        @NonNull inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {

        rootView = inflater.inflate(R.layout.activity_store_map, container, false);

        store_name.clear()
        store_address.clear()

        back_btn = rootView.findViewById(R.id.back_btn) as ImageView
        img = rootView.findViewById(R.id.img) as ImageView

        map_layout = rootView.findViewById(R.id.map_layout) as RelativeLayout

        map_list = rootView.findViewById(R.id.map_list) as RecyclerView

        back_btn.visibility = View.GONE
        back_btn.setOnClickListener {


        }

        store_name.add("Ar Rawabi")
        store_name.add("Prince Sultan Bin Abdulaziz")
        store_name.add("Al Imam Saud")
        store_name.add("Al Amir Turki")

        store_address.add("6181 Eastern Ring Branch Rd, AR Rawabi, Riyadh 14215 2475, Saudi Arabia")
        store_address.add("3507 Prince Sultan Bin Abdulaziz Rd, Al Ulaya, Riyadh 12211 7134, Saudi Arabia")
        store_address.add("8078 Al Imam Saud Ibn Abdul Aziz Branch Rd, Al Mursilat, Riyadh 12464, Saudi Arabia")
        store_address.add("6921 Al Amir Turki, Alkurnaish, Al Khobar 34414 2235, Saudi Arabia")

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment!!.getMapAsync(this)


//        (activity!!.supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?)?.let {
//            it.getMapAsync(this)
//        }

        img.setOnClickListener {

            if (map_layout.visibility == View.VISIBLE) {

                img.setImageDrawable(resources.getDrawable(R.drawable.store_map))
                map_layout.visibility = View.GONE
                map_list.visibility = View.VISIBLE

            } else {

                img.setImageDrawable(resources.getDrawable(R.drawable.store_list))
                map_layout.visibility = View.VISIBLE
                map_list.visibility = View.GONE

            }

        }



        return rootView
    }

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0

        val networkStatus: String? =
            NetworkUtil.getConnectivityStatusString(activity)
        if (!networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
            StoreDetailsApi().execute()
        } else {
//                        if (language.equals("En", ignoreCase = true)) {
            Toast.makeText(
                activity,
                R.string.str_connection_error,
                Toast.LENGTH_SHORT
            ).show()
//                        } else {
//                            Toast.makeText(
//                                getApplicationContext(),
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
        }

//        marker1 = mMap!!.addMarker(
//            MarkerOptions()
//                .position(LatLng(24.6813850402832!!, 46.777904510498!!))
//                .title("Store")
//                .snippet("6181 Eastern Ring Branch Rd, AR Rawabi, Riyadh 14215 2475, Saudi Arabia")
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.store_map_marker))
//        )
//
//        marker1.showInfoWindow()
//        marker2 = mMap!!.addMarker(
//            MarkerOptions().position(LatLng(24.6897106170654, 46.6865158081055)).title("").icon(
//                BitmapDescriptorFactory.fromResource(R.drawable.store_map_marker)
//            )
//        )
//        marker3 = mMap!!.addMarker(
//            MarkerOptions().position(LatLng(24.7557373046875, 46.6833953857422)).title("").icon(
//                BitmapDescriptorFactory.fromResource(R.drawable.store_map_marker_12)
//            )
//        )
//        marker4 = mMap!!.addMarker(
//            MarkerOptions().position(LatLng(26.2854537963867, 50.2199478149414)).title("").icon(
//                BitmapDescriptorFactory.fromResource(R.drawable.store_map_marker_12)
//            )
//        )
        mMap!!.moveCamera(
            CameraUpdateFactory.newLatLng(
                LatLng(
                    24.6813850402832!!,
                    46.777904510498!!
                )
            )
        )
        mMap!!.animateCamera(CameraUpdateFactory.zoomTo(12.0f))

        mMap!!.setOnMarkerClickListener(this)

    }

    override fun onMarkerClick(p0: Marker?): Boolean {

        if (p0!!.equals(marker1)) {

            val mLocation = Location("")
            mLocation.latitude = marker1.position.latitude
            mLocation.longitude = marker1.position.longitude

            var geocoder: Geocoder? = null
            try {
                geocoder = Geocoder(activity, Locale.getDefault())
            } catch (e: Exception) {
                e.printStackTrace()
            }

            // Address found using the Geocoder.
            // Address found using the Geocoder.
            var addresses: List<Address>? = null
            var errorMessage = ""

            try {
                addresses = geocoder!!.getFromLocation(
                    mLocation.latitude,
                    mLocation.longitude,  // In this sample, we get just a single address.
                    1
                )
            } catch (ioException: IOException) { // Catch network or other I/O problems.
                errorMessage = this.getString(R.string.service_not_available)
                Log.e("TAG", errorMessage, ioException)
            } catch (illegalArgumentException: IllegalArgumentException) { // Catch invalid latitude or longitude values.
                errorMessage = this.getString(R.string.invalid_lat_long_used)
                Log.e(
                    "TAG", errorMessage + ". " +
                            "Latitude = " + mLocation.latitude +
                            ", Longitude = " + mLocation.longitude, illegalArgumentException
                )
            }

            if (addresses == null || addresses.size == 0) {
                if (errorMessage.isEmpty()) {
                    errorMessage = getString(R.string.no_address_found)
                    Log.e("TAG", errorMessage)
                }
            } else {
                val address = addresses[0]
                Log.d("TAG", "getLocality: $address")
                //                    if (address.getSubLocality() != null) {
//                        localityTitle.setText(address.getSubLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getSubLocality());
//                    }
//                    else {
//                        localityTitle.setText(address.getLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getLocality());
//                    }

//                mbubble_text.setText("" + address.getAddressLine(0))

//                //Set prevMarker back to default color
//                val iconFactory = IconGenerator(activity)
//                iconFactory.setRotation(0)
//                iconFactory.setBackground(null)
//                val view =
//                    View.inflate(activity, R.layout.custom_dialog, null)
//                var layout: LinearLayout
//                var store_name1: TextView
//                var store_address1: TextView
//
//
//                layout = view.findViewById(R.id.layout) as LinearLayout
//
//                store_name1 = view.findViewById(R.id.store_name) as TextView
//                store_address1 = view.findViewById(R.id.store_address) as TextView
//
//                store_name1.setText("" + store_name[0])
//                store_address1.setText("" + store_address[0])
//
//                layout.setOnClickListener {
                val a = Intent(activity, MenuActivity::class.java)
                a.putExtra("address", address.getAddressLine(0))
                startActivity(a)
//                }
//
//
//                iconFactory.setContentView(view)


                Log.d("TAG", "getLocality: " + address.getAddressLine(0))
            }

        } else if (p0!!.equals(marker2)) {

            val mLocation = Location("")
            mLocation.latitude = marker2.position.latitude
            mLocation.longitude = marker2.position.longitude

            var geocoder: Geocoder? = null
            try {
                geocoder = Geocoder(activity, Locale.getDefault())
            } catch (e: Exception) {
                e.printStackTrace()
            }

            // Address found using the Geocoder.
            // Address found using the Geocoder.
            var addresses: List<Address>? = null
            var errorMessage = ""

            try {
                addresses = geocoder!!.getFromLocation(
                    mLocation.latitude,
                    mLocation.longitude,  // In this sample, we get just a single address.
                    1
                )
            } catch (ioException: IOException) { // Catch network or other I/O problems.
                errorMessage = this.getString(R.string.service_not_available)
                Log.e("TAG", errorMessage, ioException)
            } catch (illegalArgumentException: IllegalArgumentException) { // Catch invalid latitude or longitude values.
                errorMessage = this.getString(R.string.invalid_lat_long_used)
                Log.e(
                    "TAG", errorMessage + ". " +
                            "Latitude = " + mLocation.latitude +
                            ", Longitude = " + mLocation.longitude, illegalArgumentException
                )
            }

            if (addresses == null || addresses.size == 0) {
                if (errorMessage.isEmpty()) {
                    errorMessage = getString(R.string.no_address_found)
                    Log.e("TAG", errorMessage)
                }
            } else {
                val address = addresses[0]
                Log.d("TAG", "getLocality: $address")
                //                    if (address.getSubLocality() != null) {
//                        localityTitle.setText(address.getSubLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getSubLocality());
//                    }
//                    else {
//                        localityTitle.setText(address.getLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getLocality());
//                    }

//                mbubble_text.setText("" + address.getAddressLine(0))

                //Set prevMarker back to default color
                val iconFactory = IconGenerator(activity)
                iconFactory.setRotation(0)
                iconFactory.setBackground(null)
                val view =
                    View.inflate(activity, R.layout.custom_dialog, null)
                var layout: LinearLayout
                var store_name1: TextView
                var store_address1: TextView


                layout = view.findViewById(R.id.layout) as LinearLayout

                store_name1 = view.findViewById(R.id.store_name) as TextView
                store_address1 = view.findViewById(R.id.store_address) as TextView

                store_name1.setText("" + store_name[1])
                store_address1.setText("" + store_address[1])

                layout.setOnClickListener {
                    val a = Intent(activity, MenuActivity::class.java)
                    a.putExtra("address", address.getAddressLine(0))
                    startActivity(a)
                }


                iconFactory.setContentView(view)

//                val a = Intent(activity, ItemsindividualActivity::class.java)
//                a.putExtra("address", address.getAddressLine(0))
//                startActivity(a)
                Log.d("TAG", "getLocality: " + address.getAddressLine(0))
            }

        } else if (p0!!.equals(marker3)) {

            val mLocation = Location("")
            mLocation.latitude = marker3.position.latitude
            mLocation.longitude = marker3.position.longitude

            var geocoder: Geocoder? = null
            try {
                geocoder = Geocoder(activity, Locale.getDefault())
            } catch (e: Exception) {
                e.printStackTrace()
            }

            // Address found using the Geocoder.
            // Address found using the Geocoder.
            var addresses: List<Address>? = null
            var errorMessage = ""

            try {
                addresses = geocoder!!.getFromLocation(
                    mLocation.latitude,
                    mLocation.longitude,  // In this sample, we get just a single address.
                    1
                )
            } catch (ioException: IOException) { // Catch network or other I/O problems.
                errorMessage = this.getString(R.string.service_not_available)
                Log.e("TAG", errorMessage, ioException)
            } catch (illegalArgumentException: IllegalArgumentException) { // Catch invalid latitude or longitude values.
                errorMessage = this.getString(R.string.invalid_lat_long_used)
                Log.e(
                    "TAG", errorMessage + ". " +
                            "Latitude = " + mLocation.latitude +
                            ", Longitude = " + mLocation.longitude, illegalArgumentException
                )
            }

            if (addresses == null || addresses.size == 0) {
                if (errorMessage.isEmpty()) {
                    errorMessage = getString(R.string.no_address_found)
                    Log.e("TAG", errorMessage)
                }
            } else {
                val address = addresses[0]
                Log.d("TAG", "getLocality: $address")
                //                    if (address.getSubLocality() != null) {
//                        localityTitle.setText(address.getSubLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getSubLocality());
//                    }
//                    else {
//                        localityTitle.setText(address.getLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getLocality());
//                    }

//                mbubble_text.setText("" + address.getAddressLine(0))

                //Set prevMarker back to default color
                val iconFactory = IconGenerator(activity)
                iconFactory.setRotation(0)
                iconFactory.setBackground(null)
                val view =
                    View.inflate(activity, R.layout.custom_dialog, null)
                var layout: LinearLayout
                var store_name1: TextView
                var store_address1: TextView


                layout = view.findViewById(R.id.layout) as LinearLayout

                store_name1 = view.findViewById(R.id.store_name) as TextView
                store_address1 = view.findViewById(R.id.store_address) as TextView

                store_name1.setText("" + store_name[2])
                store_address1.setText("" + store_address[2])

                layout.setOnClickListener {
                    val a = Intent(activity, MenuActivity::class.java)
                    a.putExtra("address", address.getAddressLine(0))
                    startActivity(a)
                }


                iconFactory.setContentView(view)

//                val a = Intent(activity, ItemsindividualActivity::class.java)
//                a.putExtra("address", address.getAddressLine(0))
//                startActivity(a)
                Log.d("TAG", "getLocality: " + address.getAddressLine(0))
            }

        } else if (p0!!.equals(marker4)) {

            val mLocation = Location("")
            mLocation.latitude = marker4.position.latitude
            mLocation.longitude = marker4.position.longitude

            var geocoder: Geocoder? = null
            try {
                geocoder = Geocoder(activity, Locale.getDefault())
            } catch (e: Exception) {
                e.printStackTrace()
            }

            // Address found using the Geocoder.
            // Address found using the Geocoder.
            var addresses: List<Address>? = null
            var errorMessage = ""

            try {
                addresses = geocoder!!.getFromLocation(
                    mLocation.latitude,
                    mLocation.longitude,  // In this sample, we get just a single address.
                    1
                )
            } catch (ioException: IOException) { // Catch network or other I/O problems.
                errorMessage = this.getString(R.string.service_not_available)
                Log.e("TAG", errorMessage, ioException)
            } catch (illegalArgumentException: IllegalArgumentException) { // Catch invalid latitude or longitude values.
                errorMessage = this.getString(R.string.invalid_lat_long_used)
                Log.e(
                    "TAG", errorMessage + ". " +
                            "Latitude = " + mLocation.latitude +
                            ", Longitude = " + mLocation.longitude, illegalArgumentException
                )
            }

            if (addresses == null || addresses.size == 0) {
                if (errorMessage.isEmpty()) {
                    errorMessage = getString(R.string.no_address_found)
                    Log.e("TAG", errorMessage)
                }
            } else {
                val address = addresses[0]
                Log.d("TAG", "getLocality: $address")
                //                    if (address.getSubLocality() != null) {
//                        localityTitle.setText(address.getSubLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getSubLocality());
//                    }
//                    else {
//                        localityTitle.setText(address.getLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getLocality());
//                    }

//                mbubble_text.setText("" + address.getAddressLine(0))

                //Set prevMarker back to default color
                val iconFactory = IconGenerator(activity)
                iconFactory.setRotation(0)
                iconFactory.setBackground(null)
                val view =
                    View.inflate(activity, R.layout.custom_dialog, null)
                var layout: LinearLayout
                var store_name1: TextView
                var store_address1: TextView


                layout = view.findViewById(R.id.layout) as LinearLayout

                store_name1 = view.findViewById(R.id.store_name) as TextView
                store_address1 = view.findViewById(R.id.store_address) as TextView

                store_name1.setText("" + store_name[3])
                store_address1.setText("" + store_address[3])

                layout.setOnClickListener {
                    val a = Intent(activity, MenuActivity::class.java)
                    a.putExtra("address", address.getAddressLine(0))
                    startActivity(a)
                }


                iconFactory.setContentView(view)

//                val a = Intent(activity, ItemsindividualActivity::class.java)
//                a.putExtra("address", address.getAddressLine(0))
//                startActivity(a)
                Log.d("TAG", "getLocality: " + address.getAddressLine(0))
            }

        }

        return false
    }

    private fun prepareStoreDetailsJson(): String {

        val mainObj = JSONObject()
        try {

            mainObj.put("UserId", 14)
            mainObj.put("DeviceToken", "abcdefghijklmnopqrstuvwxyz")
            mainObj.put("Latitude", 24.7233943939208)
            mainObj.put("Longitude", 46.6365165710449)
            mainObj.put("orderType", 1)
            mainObj.put("onlineStores", 1)

        } catch (je: JSONException) {
            je.printStackTrace()
        }
        Log.i("TAG", "prepareDashboardJson: $mainObj")
        return mainObj.toString()
    }

    private inner class StoreDetailsApi :
        AsyncTask<String?, Int?, String?>() {
        //        ACProgressFlower dialog;
        var customDialog: AlertDialog? = null
        var inputStr: String? = null
        override fun onPreExecute() {
            super.onPreExecute()
            inputStr = prepareStoreDetailsJson()
            //            dialog = new ACProgressFlower.Builder(DriverListActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(DriverListActivity.this);
            val dialogBuilder =
                activity?.let { AlertDialog.Builder(it) }
            // ...Irrelevant code for customizing the buttons and title
            val inflater = layoutInflater
            val layout: Int = R.layout.progress_bar_alert
            val dialogView = inflater.inflate(layout, null)
            dialogBuilder!!.setView(dialogView)
            dialogBuilder.setCancelable(false)
            customDialog = dialogBuilder.create()
            customDialog!!.show()
            val lp = WindowManager.LayoutParams()
            val window = customDialog!!.getWindow()
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            lp.copyFrom(window.attributes)
            //This makes the progressDialog take up the full width
            val display = activity!!.windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            val screenWidth = size.x
            val d = screenWidth * 0.45
            lp.width = d.toInt()
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            window.attributes = lp
        }

        protected override fun doInBackground(vararg params: String?): String? {
            val apiService: APIInterface = ApiClient.client!!.create(APIInterface::class.java)
            val call: Call<StoreInfoList?>? = apiService.getStore(
                RequestBody.create(MediaType.parse("application/json"), inputStr)
            )
            call!!.enqueue(object : Callback<StoreInfoList?> {
                override fun onResponse(
                    call: Call<StoreInfoList?>,
                    response: Response<StoreInfoList?>
                ) {
                    if (response.isSuccessful()) {
                        val storeList: StoreInfoList? = response.body()
                        if (storeList!!.getStatus()) {

                            Log.i("TAG", "onResponse: " + storeList.getMessageEn())

                            val linearLayoutManager2 = LinearLayoutManager(
                                activity,
                                LinearLayoutManager.VERTICAL,
                                false
                            )
                            map_list.setLayoutManager(linearLayoutManager2)
                            madapter = StoreListAdapter(
                                activity!!,
                                storeList.getData()
                            )
                            map_list!!.setAdapter(madapter)

                            for (i in 0 until storeList.getData()!!.size) {
                                marker1 = mMap!!.addMarker(
                                    MarkerOptions()
                                        .position(LatLng(storeList.getData()!![i]!!.latitude!!.toDouble()!!, storeList.getData()!![i]!!.longitude!!.toDouble()))
                                        .title(storeList.getData()!![i]!!.nameEn)
                                        .snippet(storeList.getData()!![i]!!.addressEn)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.store_map_marker))
                                )

                                marker1.showInfoWindow()
                            }

                        } else {
                            val failureResponse: String? = storeList.getMessageEn()
//                            if (language.equals("En", ignoreCase = true)) {
                            Constants.showOneButtonAlertDialog(
                                failureResponse,
                                context!!.resources.getString(R.string.app_name),
                                context!!.resources.getString(R.string.ok),
                                activity!!
                            )
//                            } else {
//                                Constants.showOneButtonAlertDialog(
//                                    changePasswordResponse.messageAr,
//                                    context!!.resources.getString(R.string.app_name_ar),
//                                    context!!.resources.getString(R.string.ok_ar),
//                                    this@DriverListActivity
//                                )
//                            }
                        }
                    } else {
                        Log.i("TAG", "onrespon: ")
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            activity,
                            R.string.cannot_reach_server,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.cannot_reach_server_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    }
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                }

                override fun onFailure(
                    call: Call<StoreInfoList?>,
                    t: Throwable
                ) {
                    val networkStatus: String? =
                        NetworkUtil.getConnectivityStatusString(activity)
                    if (networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            activity,
                            R.string.str_connection_error,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    } else {
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            activity,
                            R.string.cannot_reach_server,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.cannot_reach_server_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    }
                    Log.i("TAG", "onFailure: " + t)
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                }
            })
            return null
        }
    }

}