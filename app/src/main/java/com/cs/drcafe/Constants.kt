package com.cs.drcafe

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.Point
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.provider.Settings
import android.provider.Settings.SettingNotFoundException
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import java.util.*

class Constants {

    //    public static ArrayList<ItemsList.Items> ItemsArrayList = new ArrayList<>();
    //    public static ArrayList<ItemsList.Banners> BannersArrayList = new ArrayList<>();


    object LocationConstants {
        const val SUCCESS_RESULT = 0
        const val FAILURE_RESULT = 1
        const val PACKAGE_NAME = "com.cs.drcafe."
        const val RECEIVER = "$PACKAGE_NAME.RECEIVER"
        const val RESULT_DATA_KEY = "$PACKAGE_NAME.RESULT_DATA_KEY"
        const val LOCATION_DATA_EXTRA =
            "$PACKAGE_NAME.LOCATION_DATA_EXTRA"
        const val LOCATION_DATA_AREA =
            "$PACKAGE_NAME.LOCATION_DATA_AREA"
        const val LOCATION_DATA_CITY =
            "$PACKAGE_NAME.LOCATION_DATA_CITY"
        const val LOCATION_DATA_STREET =
            "$PACKAGE_NAME.LOCATION_DATA_STREET"
    }

    companion object {

        var str_address: String = ""
        var Country_Code: String = "+966 "
        var lat : Double = 0.0
        var long : Double = 0.0

        var IMAGE_URL = "http://csadms.com/dcweb/admin/Uploads/"

        fun isLocationEnabled(context: Context): Boolean {
            var locationMode = 0
            val locationProviders: String
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                try {
                    locationMode = Settings.Secure.getInt(
                        context.contentResolver,
                        Settings.Secure.LOCATION_MODE
                    )
                } catch (e: SettingNotFoundException) {
                    e.printStackTrace()
                }
                locationMode != Settings.Secure.LOCATION_MODE_OFF
            } else {
                locationProviders = Settings.Secure.getString(
                    context.contentResolver,
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED
                )
                !TextUtils.isEmpty(locationProviders)
            }
        }

        fun requestEditTextFocus(view: View, activity: Activity) {
            if (view.requestFocus()) {
                activity.window
                    .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
            }
        }

        fun showOneButtonAlertDialog(
            descriptionStr: String?,
            titleStr: String?,
            buttonStr: String?,
            context: Activity
        ) {
            val languagePrefs: SharedPreferences
            val language: String?
            languagePrefs =
                context!!.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE)
            language = languagePrefs.getString("language", "En")
            var customDialog: AlertDialog? = null
            val dialogBuilder =
                AlertDialog.Builder(context)
            // ...Irrelevant code for customizing the buttons and title
            val inflater = context!!.layoutInflater
            val layout: Int
//            layout = if (language.equals("En", ignoreCase = true)) {
               layout = R.layout.alert_dialog
//            } else {
//                R.layout.alert_dialog_arabic
//            }
            val dialogView = inflater.inflate(layout, null)
            dialogBuilder.setView(dialogView)
            dialogBuilder.setCancelable(false)
            val title = dialogView.findViewById<View>(R.id.title) as TextView
            val desc = dialogView.findViewById<View>(R.id.desc) as TextView
            val yes = dialogView.findViewById<View>(R.id.pos_btn) as TextView
            val no = dialogView.findViewById<View>(R.id.ngt_btn) as TextView
            val vert =
                dialogView.findViewById(R.id.vert_line) as View
            no.visibility = View.GONE
            vert.visibility = View.GONE
            title.text = titleStr
            yes.text = buttonStr
            desc.text = descriptionStr
            customDialog = dialogBuilder.create()
            customDialog.show()
            val finalCustomDialog = customDialog
            yes.setOnClickListener { finalCustomDialog!!.dismiss() }
            val lp = WindowManager.LayoutParams()
            val window = customDialog.window
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            lp.copyFrom(window.attributes)
            //This makes the dialog take up the full width
            val display = context.windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            val screenWidth = size.x
            val d = screenWidth * 0.85
            lp.width = d.toInt()
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            window.attributes = lp
        }

    }

    fun getTypeFace(context: Context): Typeface? {
        return Typeface.createFromAsset(
            context.assets,
            "helvetica.ttf"
        )
    }


    fun convertToArabic(value: String): String? {
        return value
            .replace("١".toRegex(), "1").replace("٢".toRegex(), "2")
            .replace("٣".toRegex(), "3").replace("٤".toRegex(), "4")
            .replace("٥".toRegex(), "5").replace("٦".toRegex(), "6")
            .replace("٧".toRegex(), "7").replace("٨".toRegex(), "8")
            .replace("٩".toRegex(), "9").replace("٠".toRegex(), "0")
            .replace("٫".toRegex(), ".")
    }

    // HyperPay
    /* The configuration values to change across the app */
    object Config {
        /* The payment brands for Ready-to-Use UI and Payment Button */
        var PAYMENT_BRANDS: MutableSet<String>? = null

        init {
            var PAYMENT_BRANDS: MutableSet<String>? = null
            PAYMENT_BRANDS = LinkedHashSet()
            PAYMENT_BRANDS.add("VISA")
            PAYMENT_BRANDS.add("MASTER")
            PAYMENT_BRANDS.add("MADA")
        }
    }

    val SHOPPER_RESULT_URL = "com.cs.bakeryco.payments://result"
}