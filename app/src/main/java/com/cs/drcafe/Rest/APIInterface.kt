package com.cs.bcvendor.Rest

import com.cs.drcafe.Models.*
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface APIInterface {

    @POST("Address/SaveNewAddress")
    fun getSaveAddress(@Body body: RequestBody?): Call<SaveAddressList?>?

    @POST("Address/GetAddress")
    fun getAddresslist(@Body body: RequestBody?): Call<AddressList?>?

    @POST("Address/UpdateAddress")
    fun getUpdateAddress(@Body body: RequestBody?): Call<UpdateAddressList?>?

    @POST("Address/DeleteAddress")
    fun getDeleteAddress(@Body body: RequestBody?): Call<DeleteAddressList?>?

    @POST("Dashboard/GetDashboard")
    fun getDashboard(@Body body: RequestBody?): Call<DashboardList?>?

    @POST("Store/GetStoreList")
    fun getStore(@Body body: RequestBody?): Call<StoreInfoList?>?

    @POST("Store/StoreCategories")
    fun getStore_category(@Body body: RequestBody?): Call<StoreCategoryList?>?

}