package com.cs.drcafe

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.cs.drcafe.Fragments.HomeFragment
import com.cs.drcafe.Fragments.OrderFragment
import com.cs.drcafe.Fragments.StoreFragment
import com.cs.drcafe.Fragments.WalletFragment
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.lang.reflect.Field

class MainActivity : AppCompatActivity() {
    var fragmentManager: FragmentManager = getSupportFragmentManager()
    var language: String? = null
    var userId: String? = null
    var userPrefs: SharedPreferences? = null
    var languagePrefs: SharedPreferences? = null
    var format: String? = null
    private val mOnNavigationItemSelectedListener: BottomNavigationView.OnNavigationItemSelectedListener =
        object : BottomNavigationView.OnNavigationItemSelectedListener {
            @SuppressLint("ResourceType")
            override fun onNavigationItemSelected(@NonNull item: MenuItem): Boolean {
                when (item.itemId) {
                    R.id.navigation_home -> {
                        val mainFragment: HomeFragment = HomeFragment()
                        fragmentManager.beginTransaction()
                            .replace(R.id.fragment_layout, mainFragment).commit()
                        item.setIcon(R.drawable.menu_home_selceted)
                        val menu1: Menu = navigation!!.getMenu()
                        return true
                    }
                    R.id.navigation_store -> {
                        item.setIcon(R.drawable.menu_store)
                        val storeFragment: StoreFragment = StoreFragment()
                        fragmentManager.beginTransaction()
                            .replace(R.id.fragment_layout, storeFragment).commit()
                        return true
                    }
                    R.id.navigation_order -> {
                        item.setIcon(R.drawable.menu_order)
                        val accountFragment: OrderFragment = OrderFragment()
                        fragmentManager.beginTransaction()
                            .replace(R.id.fragment_layout, accountFragment).commit()
                        return true
                    }
                    R.id.navigation_wallet -> {
                        item.setIcon(R.drawable.menu_wallet)
                        val accountFragment: WalletFragment = WalletFragment()
                        fragmentManager.beginTransaction()
                            .replace(R.id.fragment_layout, accountFragment).commit()
                        return true
                    }
                    R.id.navigation_setting -> {
                        item.setIcon(R.drawable.menu_setting)
//                        val accountFragment: ProfileScreen = ProfileScreen()
//                        fragmentManager.beginTransaction()
//                            .replace(R.id.fragment_layout, accountFragment).commit()
                        return true
                    }
                }
                return false
            }
        }

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation = findViewById(R.id.navigation) as BottomNavigationView?
        navigation!!.setOnNavigationItemSelectedListener(
            mOnNavigationItemSelectedListener
        )
        disableShiftMode(navigation!!)
//        navigation!!.itemIconTintList = null
        val menu: Menu = navigation!!.getMenu()
//        if (language.equals("En", ignoreCase = true)) {
        val menuItem = menu.getItem(0)
        navigation!!.setSelectedItemId(menuItem.itemId)
//        } else {
//            val menuItem = menu.getItem(1)
//            navigation!!.setSelectedItemId(menuItem.itemId)
//        }
        val mainFragment: HomeFragment = HomeFragment()
        fragmentManager.beginTransaction()
            .replace(R.id.fragment_layout, mainFragment).commit()
        //            Fragment mainFragment = new OrderStatus();
//            fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

    }

    @SuppressLint("RestrictedApi")
    private fun disableShiftMode(view: BottomNavigationView) {
        val menuView: BottomNavigationMenuView = view.getChildAt(0) as BottomNavigationMenuView
        try {
            val shiftingMode: Field =
                menuView.javaClass.getDeclaredField("mShiftingMode")
            shiftingMode.isAccessible = true
            shiftingMode.setBoolean(menuView, false)
            shiftingMode.isAccessible = false
            for (i in 0 until menuView.getChildCount()) {
                val item: BottomNavigationItemView =
                    menuView.getChildAt(i) as BottomNavigationItemView
                //                item.setShiftingMode(false);
                item.setPadding(0, 15, 0, 0)
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked())
            }
        } catch (e: NoSuchFieldException) {
            Log.e("BNVHelper", "Unable to get shift mode field", e)
        } catch (e: IllegalAccessException) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e)
        }
    }

    @SuppressLint("ResourceType")
    protected override fun onResume() {
        super.onResume()
        val menu: Menu = navigation!!.getMenu()
    }

    companion object {
        private const val ACCOUNT_INTENT = 1
        var navigation: BottomNavigationView? = null
    }
}
