package com.cs.drcafe.Activities

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Point
import android.location.Location
import android.os.*
import android.provider.Settings
import android.text.Editable
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.location.LocationManagerCompat.isLocationEnabled
import com.cs.drcafe.Constants
import com.cs.drcafe.R
import com.cs.drcafe.Widgets.FetchAddressIntentService
import com.cs.drcafe.Widgets.GPSTracker
import com.google.android.gms.common.*
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_map_view.*
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

class MapViewActivity : AppCompatActivity(), OnMapReadyCallback,
    ConnectionCallbacks, OnConnectionFailedListener,
    LocationListener {
    var gps: GPSTracker? = null
    var context: Context? = null
    var lat: Double? = null
    var longi: Double? = null
    private var mMap: GoogleMap? = null
    protected var mAreaOutput: String? = null
    protected var mCityOutput: String? = null
    protected var mStateOutput: String? = null
    //    private RelativeLayout titleLayout;
    private var backButton1: ImageView? = null
    protected var mAddressOutput: String? = null
    private var mConfirmLocation: Button? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    lateinit var mLocationAddress: TextView

    //    EditText inputContactPerson, inputContactNo;

    //    TextInputLayout inputContactPersonLayout, inputContactNoLayout;
    private var mResultReceiver: AddressResultReceiver? =
        null

    //    private String  strContactperson, strContactNo;
    private var strAddress: String? = null
    private var strHouseNumber: String? = null
    private var strLandmark: String? = null
    private var strHouseName: String? = null
    private var strAddressType: String? = null
    var mapFragment: SupportMapFragment? = null
    var language: String? = null
    var userId: String? = null
    var userPrefs: SharedPreferences? = null
    var LanguagePrefs: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_map_view)
        context = this
        mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?

        mLocationAddress = findViewById(R.id.location) as TextView
        backButton1 = findViewById(R.id.back_btn) as ImageView

        backButton1!!.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
        mapFragment!!.getMapAsync(this)

        save.setOnClickListener {

            Constants.lat = this!!.lat!!
            Constants.long = this!!.longi!!

            val a = Intent(this@MapViewActivity, AddAddressActivity::class.java)
            startActivity(a)
            finish()

        }

        mResultReceiver =
            AddressResultReceiver(Handler())
        if (checkPlayServices()) {
            // If this check succeeds, proceed with normal processing.
            // Otherwise, prompt user to get valid Play Services APK.
            if (!Constants.isLocationEnabled(this)) {
                var customDialog: AlertDialog? = null
                val dialogBuilder =
                    AlertDialog.Builder(this)
                // ...Irrelevant code for customizing the buttons and title
                val inflater = layoutInflater
                var layout = 0
                layout =
                    R.layout.alert_dialog
                val dialogView = inflater.inflate(layout, null)
                dialogBuilder.setView(dialogView)
                dialogBuilder.setCancelable(false)
                val desc =
                    dialogView.findViewById<View>(R.id.desc) as TextView
                val yes =
                    dialogView.findViewById<View>(R.id.pos_btn) as TextView
                val no =
                    dialogView.findViewById<View>(R.id.ngt_btn) as TextView
                val vert =
                    dialogView.findViewById(R.id.vert_line) as View
                if (language.equals("En", ignoreCase = true)) {
                    yes.text = "Open location settings"
                    no.text = "Cancel"
                    desc.text = "Location not enabled!"
                } else {
                    yes.text = "افتح ضبط الموقع"
                    no.text = "الغاء"
                    desc.text = "افتح ضبط الموقع"
                }
                yes.setOnClickListener {
                    val myIntent =
                        Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivity(myIntent)
                    customDialog!!.dismiss()
                }
                no.setOnClickListener { customDialog!!.dismiss() }
                customDialog = dialogBuilder.create()
                customDialog.show()
                val lp = WindowManager.LayoutParams()
                val window = customDialog.getWindow()
                lp.copyFrom(window!!.attributes)
                //This makes the dialog take up the full width
                val display = windowManager.defaultDisplay
                val size = Point()
                display.getSize(size)
                val screenWidth = size.x
                val d = screenWidth * 0.85
                lp.width = d.toInt()
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT
                window.attributes = lp
            }
            buildGoogleApiClient()
        } else {
            if (language.equals("En", ignoreCase = true)) {
                Toast.makeText(
                    context,
                    "Location not supported in this device",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    context,
                    "تم رفض الموقع، لا يمكن إظهار المتاجر القريبة",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.animateCamera(CameraUpdateFactory.zoomTo(15f))
        val currentapiVersion = Build.VERSION.SDK_INT
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        LOCATION_PERMS,
                        LOCATION_REQUEST
                    )
                }
            } else {
                gPSCoordinates
            }
        } else {
            gPSCoordinates
        }
        val mapView = mapFragment!!.view
        val locationButton =
            (mapView!!.findViewById<View>("1".toInt())
                .parent as View)
                .findViewById<View>("2".toInt())
        val rlp =
            locationButton.layoutParams as RelativeLayout.LayoutParams
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        rlp.setMargins(0, 0, 30, 30)
        mMap!!.setOnCameraChangeListener { cameraPosition ->
            try {
                var mCenterLatLong: LatLng? = null
                mCenterLatLong = cameraPosition.target
                val mLocation = Location("")
                mLocation.latitude = mCenterLatLong.latitude
                mLocation.longitude = mCenterLatLong.longitude
                lat = mCenterLatLong.latitude
                longi = mCenterLatLong.longitude
                startIntentService(mLocation)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
    }// can't get location
    // GPS or Network is not enabled
    // Ask user to enable GPS/network in settings

    // TODO: Consider calling
    //    ActivityCompat#requestPermissions
    // here to request the missing permissions, and then overriding
    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
    //                                          int[] grantResults)
    // to handle the case where the user grants the permission. See the documentation
    // for ActivityCompat#requestPermissions for more details.
    val gPSCoordinates: Unit
        get() {
            gps = GPSTracker(this)
            try {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return
                }
                mMap!!.isMyLocationEnabled = true
            } catch (npe: Exception) {
                npe.printStackTrace()
            }
            if (gps != null) {
                if (gps!!.canGetLocation()) {

                    lat = gps!!.getLatitude()
                    longi = gps!!.getLongitude()

                    val latLng = LatLng(lat!!, longi!!)
                    mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
                    mMap!!.animateCamera(CameraUpdateFactory.zoomTo(15f))
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps!!.showSettingsAlert()
                }
            }
        }

    private fun canAccessLocation(): Boolean {
        return hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    }

    private fun canAccessLocation1(): Boolean {
        return hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
    }

    private fun hasPermission(perm: String): Boolean {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
            this,
            perm
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            LOCATION_REQUEST -> if (canAccessLocation()) {
                gPSCoordinates
            } else {
                if (language.equals("En", ignoreCase = true)) {
                    Toast.makeText(
                        this,
                        "Location permission denied, Unable to show nearby stores",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    Toast.makeText(
                        this,
                        "خطأ في الموقع لايمكن عرض المتجر",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    override fun onConnected(bundle: Bundle?) {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        val mLastLocation =
            LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient
            )
        if (mLastLocation != null) {
            changeMap(mLastLocation)
            Log.d(
                TAG,
                "ON connected"
            )
        } else {
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        try {
            val mLocationRequest = LocationRequest()
            mLocationRequest.interval = 10000
            mLocationRequest.fastestInterval = 5000
            mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onConnectionSuspended(i: Int) {
        Log.i(
            TAG,
            "Connection suspended"
        )
        mGoogleApiClient!!.connect()
    }

    override fun onLocationChanged(location: Location) {
        try {
            Log.d(
                TAG,
                "onLocationChanged: "
            )
            if (location != null) {
                changeMap(location)
                LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
    }

    override fun onStart() {
        super.onStart()
        try {
            mGoogleApiClient!!.connect()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected) {
            mGoogleApiClient!!.disconnect()
        }
    }

    private fun checkPlayServices(): Boolean {
        val resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this)
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(
                    resultCode, this,
                    PLAY_SERVICES_RESOLUTION_REQUEST
                ).show()
            } else {
                //finish();
            }
            return false
        }
        return true
    }

    private fun changeMap(location: Location) {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }

        // check if map is created successfully or not
        if (mMap != null) {
            mMap!!.uiSettings.isZoomControlsEnabled = false
            val latLong: LatLng
            latLong = LatLng(location.latitude, location.longitude)
            val cameraPosition = CameraPosition.Builder()
                .target(latLong).zoom(15f).build()
            mMap!!.isMyLocationEnabled = true
            mMap!!.uiSettings.isMyLocationButtonEnabled = true
            mMap!!.animateCamera(
                CameraUpdateFactory
                    .newCameraPosition(cameraPosition)
            )
            lat = location.latitude
            longi = location.longitude

//            mLocationMarkerText.setText("Lat : " + location.getLatitude() + "," + "Long : " + location.getLongitude());
            startIntentService(location)
        } else {
            Toast.makeText(
                applicationContext,
                "Sorry! unable to create maps", Toast.LENGTH_SHORT
            )
                .show()
        }
    }

    /**
     * Receiver for data sent from FetchAddressIntentService.
     */
    internal inner class AddressResultReceiver(handler: Handler?) :
        ResultReceiver(handler) {
        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        override fun onReceiveResult(
            resultCode: Int,
            resultData: Bundle
        ) {

            // Display the address string or an error message sent from the intent service.
            mAddressOutput = resultData.getString(Constants.LocationConstants.RESULT_DATA_KEY)
            mAreaOutput = resultData.getString(Constants.LocationConstants.LOCATION_DATA_AREA)
            mCityOutput = resultData.getString(Constants.LocationConstants.LOCATION_DATA_CITY)
            mStateOutput =
                resultData.getString(Constants.LocationConstants.LOCATION_DATA_STREET)
            Log.d(
                TAG,
                "onReceiveResult: " + mStateOutput
            )
            displayAddressOutput()

            // Show a toast message if an address was found.
            if (resultCode == Constants.LocationConstants.SUCCESS_RESULT) {
                //  showToast(getString(R.string.address_found));
            }
        }
    }

    /**
     * Updates the address in the UI.
     */
    protected fun displayAddressOutput() {
        try {
            if (mStateOutput != null) Log.i(
                TAG,
                "displayAddressOutput: $mStateOutput"
            )
            mLocationAddress!!.setText(mStateOutput)
            Constants.str_address = this!!.mStateOutput.toString();
            //            mLocationAddress.setSelection(mLocationAddress.length());
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Creates an intent, adds location data to it as an extra, and starts the intent service for
     * fetching an address.
     */
    protected fun startIntentService(mLocation: Location?) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        val intent = Intent(this, FetchAddressIntentService::class.java)
        Log.d(
            TAG,
            "startIntentService: " + mResultReceiver
        )
        // Pass the result receiver as an extra to the service.
        intent.putExtra(Constants.LocationConstants.RECEIVER, mResultReceiver)

        // Pass the location data as an extra to the service.
        intent.putExtra(Constants.LocationConstants.LOCATION_DATA_EXTRA, mLocation)

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        startService(intent)
    }

    private fun openAutocompleteActivity() {
        Log.d(
            TAG,
            "openAutocompleteActivity: "
        )
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Log.d(
                TAG,
                "openAutocompleteActivity: "
            )
            val intent =
                PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .build(this)
            startActivityForResult(
                intent,
                REQUEST_CODE_AUTOCOMPLETE
            )
        } catch (e: GooglePlayServicesRepairableException) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            e.printStackTrace()
            GoogleApiAvailability.getInstance().getErrorDialog(
                this, e.connectionStatusCode,
                0 /* requestCode */
            ).show()
            Log.i("TAG", "openAutocompleteActivity1: ")
        } catch (e: GooglePlayServicesNotAvailableException) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            val message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode)
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Called after the autocomplete activity has finished to return its result.
     */
    public override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("TAG", "onActivityResult: ")
        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == Activity.RESULT_OK) {
                Log.i("TAG", "RESULT_OK: ")
                // Get the user's selected place from the Intent.
                val place = PlaceAutocomplete.getPlace(context, data)

                // TODO call location based filter
                val latLong: LatLng
                latLong = place.latLng
                val cameraPosition = CameraPosition.Builder()
                    .target(latLong).zoom(15f).build()
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return
                }
                mMap!!.isMyLocationEnabled = true
                mMap!!.animateCamera(
                    CameraUpdateFactory
                        .newCameraPosition(cameraPosition)
                )
            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            val status =
                PlaceAutocomplete.getStatus(context, data)
        } else if (resultCode == Activity.RESULT_CANCELED) {
        }
    }

    companion object {
        private const val TAG = "TAG"
        private const val REQUEST_CODE_AUTOCOMPLETE = 1
        private const val PLAY_SERVICES_RESOLUTION_REQUEST = 9000
        private val LOCATION_PERMS = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        private const val INITIAL_REQUEST = 1337
        private const val LOCATION_REQUEST = 3

        @Throws(IOException::class)
        private fun convertInputStreamToString(inputStream: InputStream): String? {
            val bufferedReader =
                BufferedReader(InputStreamReader(inputStream))
            var line: String? = ""
            var result: String? = ""
            while (bufferedReader.readLine().also { line = it } != null) result += line
            inputStream.close()
            return result
        }
    }
}
