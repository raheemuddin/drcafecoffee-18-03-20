package com.cs.drcafe.Activities

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Adapters.AdditionalDrinkListAdapter
import com.cs.drcafe.R

class AdditionalV12Activity : AppCompatActivity() {

    lateinit var back_btn: ImageView
    lateinit var selected_1: ImageView
    lateinit var selected_2: ImageView
    lateinit var selected_3: ImageView
    lateinit var selected_4: ImageView
    lateinit var check_out: LinearLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_additional_type_2)


        back_btn = findViewById(R.id.back_btn) as ImageView
        selected_1 = findViewById(R.id.selected_1) as ImageView
        selected_2 = findViewById(R.id.selected_2) as ImageView
        selected_3 = findViewById(R.id.selected_3) as ImageView
        selected_4 = findViewById(R.id.selected_4) as ImageView

        check_out = findViewById(R.id.check_out) as LinearLayout

        back_btn.setOnClickListener {

            finish()

        }

        selected_1.setOnClickListener {

            selected_1.setImageDrawable(resources.getDrawable(R.drawable.add_selected))
            selected_2.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_3.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_4.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))

        }

        selected_2.setOnClickListener {

            selected_1.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_2.setImageDrawable(resources.getDrawable(R.drawable.add_selected))
            selected_3.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_4.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))

        }

        selected_3.setOnClickListener {

            selected_1.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_2.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_3.setImageDrawable(resources.getDrawable(R.drawable.add_selected))
            selected_4.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))

        }

        selected_4.setOnClickListener {

            selected_1.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_2.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_3.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_4.setImageDrawable(resources.getDrawable(R.drawable.add_selected))

        }

        check_out.setOnClickListener {

            val a = Intent(this, ConfirmationActivity::class.java)
            startActivity(a)

        }


    }

}