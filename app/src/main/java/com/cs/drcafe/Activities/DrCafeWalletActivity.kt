package com.cs.drcafe.Activities

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Adapters.DCCardListAdapter
import com.cs.drcafe.R

class DrCafeWalletActivity : AppCompatActivity() {

    lateinit var back_btn: ImageView
    lateinit var dccard_list: RecyclerView
    lateinit var madapter: DCCardListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dr_cafe_card)

        back_btn = findViewById(R.id.back_btn) as ImageView
        dccard_list = findViewById(R.id.dr_card_list) as RecyclerView
        back_btn.setOnClickListener {

            finish()

        }

        val linearLayoutManager1 = LinearLayoutManager(
            this@DrCafeWalletActivity,
            LinearLayoutManager.VERTICAL,
            false
        )

        dccard_list!!.setLayoutManager(linearLayoutManager1)
        madapter = DCCardListAdapter(
            this@DrCafeWalletActivity
        )
        dccard_list!!.setAdapter(madapter)

    }

}