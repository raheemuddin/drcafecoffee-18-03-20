package com.cs.drcafe.Activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.cs.drcafe.MainActivity
import com.cs.drcafe.R

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

        Handler().postDelayed({
            // This method will be executed once the timer is over
           // Start your app main activity

            val i = Intent(this@SplashScreenActivity, MainActivity::class.java)
            //                i.putExtra("class","splash");
            startActivity(i)

            // close this activity
            finish()
        }, 1000)

    }

}