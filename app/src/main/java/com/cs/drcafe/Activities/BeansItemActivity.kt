package com.cs.drcafe.Activities

import android.content.Intent
import android.media.Image
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Adapters.BeansItemAdapter
import com.cs.drcafe.R

class BeansItemActivity : AppCompatActivity() {

    lateinit var items_list: RecyclerView
    lateinit var back_btn: ImageView
    var items_lists: ArrayList<String> = ArrayList()
    lateinit var madapter: BeansItemAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beans_item)

        items_lists.clear()

        items_lists.add("The Original mini pak")
        items_lists.add("The Original Standard pak")
        items_lists.add("The Original pods")

        items_list = findViewById(R.id.items_list) as RecyclerView
        back_btn = findViewById(R.id.back_btn) as ImageView

        back_btn.setOnClickListener {

            finish()

        }

        val linearLayoutManager2 = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )

        items_list.layoutManager = linearLayoutManager2
        madapter = BeansItemAdapter(this, items_lists)
        items_list.adapter = madapter

    }

}