package com.cs.drcafe.Activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Adapters.StoreFeatureAdapter
import com.cs.drcafe.R

class StoreDetailsActivity : AppCompatActivity() {

    lateinit var store_list : RecyclerView
    lateinit var madapter : StoreFeatureAdapter
    var image : ArrayList<Int> = ArrayList()
    var feature_name : ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_info)

        store_list = findViewById(R.id.store_list) as RecyclerView

        image.clear()
        feature_name.clear()

        image.add(R.drawable.store_details_wifi)
        image.add(R.drawable.store_details_drive_thr)
        image.add(R.drawable.store_details_family)
        image.add(R.drawable.store_details_meeting)
        image.add(R.drawable.store_details_patio)
        image.add(R.drawable.store_details_univer)
        image.add(R.drawable.store_details_hospital)
        image.add(R.drawable.store_details_shopping)
        image.add(R.drawable.store_details_shopping_mall)
        image.add(R.drawable.store_details_airport)

        feature_name.add("WiFi")
        feature_name.add("Drive Thru")
        feature_name.add("Family Section")
        feature_name.add("Meeting Room")
        feature_name.add("Patio Sitting")
        feature_name.add("University")
        feature_name.add("Hospital");
        feature_name.add("Shopping Mall")
        feature_name.add("Shopping Mall")
        feature_name.add("Airport")

        var linearLayoutManager = GridLayoutManager(this, 4)
        store_list.layoutManager = linearLayoutManager

        madapter = StoreFeatureAdapter(this, image, feature_name)
        store_list.adapter = madapter

    }

}