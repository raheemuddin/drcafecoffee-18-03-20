package com.cs.drcafe.Activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Adapters.ConfirmItemsListAdapter
import com.cs.drcafe.Adapters.ConfirmRecommendedAdapter
import com.cs.drcafe.Adapters.ItemListAdapter
import com.cs.drcafe.R
import kotlinx.android.synthetic.main.activity_confirmation.*

class ConfirmationActivity : AppCompatActivity() {

    lateinit var confirm_list: RecyclerView
    lateinit var madapter: ConfirmItemsListAdapter

    lateinit var recommend_list: RecyclerView
    lateinit var madapter1: ConfirmRecommendedAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)

        confirm_list = findViewById(R.id.confirm_list) as RecyclerView
        recommend_list = findViewById(R.id.recommend_list) as RecyclerView

        back_btn.setOnClickListener {

            finish()

        }

        val linearLayoutManager1 = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
        confirm_list!!.setLayoutManager(linearLayoutManager1)
        madapter = ConfirmItemsListAdapter(
            this
        )
        confirm_list!!.setAdapter(madapter)

        val linearLayoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        var mqty : ArrayList<Int> = ArrayList()

        for (i in 0 until 5){
            mqty.add(0)
        }

        recommend_list!!.setLayoutManager(linearLayoutManager)
        madapter1 = ConfirmRecommendedAdapter(
            this, mqty
        )
        recommend_list!!.setAdapter(madapter1)

        confirm.setOnClickListener {

            val a = Intent(this, TrackOrderActivity::class.java)
            startActivity(a)

        }

    }

}