package com.cs.drcafe.Activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Adapters.AdditionalDrinkListAdapter
import com.cs.drcafe.Adapters.RecommendedAdapter
import com.cs.drcafe.R

class AdditionalActivity : AppCompatActivity() {

    lateinit var back_btn: ImageView
    lateinit var selected_1: ImageView
    lateinit var selected_2: ImageView
    lateinit var selected_3: ImageView
    lateinit var selected_4: ImageView
    lateinit var drink_list: RecyclerView
    lateinit var list_view: RecyclerView
    lateinit var added_layout: LinearLayout
    lateinit var added_layout1: LinearLayout
    lateinit var add_layout2: LinearLayout
    lateinit var no_thanks_layout: LinearLayout
    lateinit var add_more_layout: LinearLayout
    lateinit var add_bag: LinearLayout
    var pricedrinkList: ArrayList<String> = ArrayList()
    var sizedrinkList: ArrayList<String> = ArrayList()
    lateinit var madapter: AdditionalDrinkListAdapter
    lateinit var madapter1: RecommendedAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_additional)

        pricedrinkList.clear()
        sizedrinkList.clear()

        back_btn = findViewById(R.id.back_btn) as ImageView
        selected_1 = findViewById(R.id.selected_1) as ImageView
        selected_2 = findViewById(R.id.selected_2) as ImageView
        selected_3 = findViewById(R.id.selected_3) as ImageView
        selected_4 = findViewById(R.id.selected_4) as ImageView
        drink_list = findViewById(R.id.drink_list) as RecyclerView
        list_view = findViewById(R.id.list_view) as RecyclerView

        added_layout = findViewById(R.id.added_layout) as LinearLayout
        added_layout1 = findViewById(R.id.added_layout1) as LinearLayout
        add_layout2 = findViewById(R.id.add_layout2) as LinearLayout
        no_thanks_layout = findViewById(R.id.no_thanks_layout) as LinearLayout
        add_more_layout = findViewById(R.id.add_more_layout) as LinearLayout
        add_bag = findViewById(R.id.add_bag) as LinearLayout

        sizedrinkList.add("Size")
        sizedrinkList.add("Coffee Box")
        sizedrinkList.add("Jumbo")
        sizedrinkList.add("Grende")
        sizedrinkList.add("Tail")
        sizedrinkList.add("Short")

        pricedrinkList.add("Price")
        pricedrinkList.add("85.00")
        pricedrinkList.add("28.00")
        pricedrinkList.add("16.00")
        pricedrinkList.add("14.00")
        pricedrinkList.add("12.00")

        back_btn.setOnClickListener {

            finish()

        }

        add_bag.setOnClickListener {

            added_layout.visibility = View.VISIBLE
            added_layout1.visibility = View.GONE
            add_layout2.visibility = View.GONE
            no_thanks_layout.visibility = View.VISIBLE
            add_more_layout.visibility = View.GONE

        }

        no_thanks_layout.setOnClickListener {

            added_layout.visibility = View.VISIBLE
            added_layout1.visibility = View.GONE
            add_layout2.visibility = View.GONE
            no_thanks_layout.visibility = View.GONE
            add_more_layout.visibility = View.VISIBLE

        }

        selected_1.setOnClickListener {

            selected_1.setImageDrawable(resources.getDrawable(R.drawable.add_selected))
            selected_2.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_3.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_4.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))

        }

        selected_2.setOnClickListener {

            selected_1.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_2.setImageDrawable(resources.getDrawable(R.drawable.add_selected))
            selected_3.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_4.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))

        }

        selected_3.setOnClickListener {

            selected_1.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_2.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_3.setImageDrawable(resources.getDrawable(R.drawable.add_selected))
            selected_4.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))

        }

        selected_4.setOnClickListener {

            selected_1.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_2.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_3.setImageDrawable(resources.getDrawable(R.drawable.add_unselected))
            selected_4.setImageDrawable(resources.getDrawable(R.drawable.add_selected))

        }

        val linearLayoutManager2 = LinearLayoutManager(
            this@AdditionalActivity,
            LinearLayoutManager.VERTICAL,
            false
        )

        drink_list!!.setLayoutManager(linearLayoutManager2)
        madapter = AdditionalDrinkListAdapter(
            this@AdditionalActivity,
            sizedrinkList, pricedrinkList
        )
        drink_list!!.setAdapter(madapter)

        val linearLayoutManager1 = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
        var mqty: ArrayList<Int> = ArrayList()

        for (i in 0 until 2) {
            mqty.add(0)
        }

        list_view!!.setLayoutManager(linearLayoutManager1)
        madapter1 = RecommendedAdapter(
            this, mqty
        )
        list_view!!.setAdapter(madapter1)

        LocalBroadcastManager.getInstance(this).registerReceiver(
            Recommended, IntentFilter("recommended_added")
        )

    }

    private val Recommended: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(
            context: Context,
            intent: Intent
        ) {

            added_layout.visibility = View.VISIBLE
            added_layout1.visibility = View.GONE
            add_layout2.visibility = View.GONE
            no_thanks_layout.visibility = View.GONE
            add_more_layout.visibility = View.VISIBLE

        }

    }
}