package com.cs.drcafe.Activities

import android.content.Context
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.bcvendor.Rest.APIInterface
import com.cs.bcvendor.Rest.ApiClient
import com.cs.drcafe.Adapters.MenuListAdapter
import com.cs.drcafe.Constants
import com.cs.drcafe.Models.StoreCategoryList
import com.cs.drcafe.R
import com.cs.drcafe.Widgets.NetworkUtil
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MenuActivity : AppCompatActivity() {

    lateinit var menu_header: RecyclerView
    lateinit var items_list: RecyclerView
    lateinit var view: View
    lateinit var str_address: String
    lateinit var address: TextView
    lateinit var back_btn: ImageView
    var main_cat_list: ArrayList<String> = ArrayList()
    var cat_list: ArrayList<String> = ArrayList()
    var cat_img: ArrayList<Int> = ArrayList()
    lateinit var madapter: MenuListAdapter
    lateinit var menu_img: ImageView
    lateinit var context: Context
    var store_id: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item)

        str_address = intent.getStringExtra("address")
        store_id = intent.getIntExtra("store_id", 0)

        context = this

        menu_header = findViewById(R.id.menu_list) as RecyclerView
        items_list = findViewById(R.id.items_list) as RecyclerView
        view = findViewById(R.id.view) as View
        address = findViewById(R.id.address) as TextView
        back_btn = findViewById(R.id.back_btn) as ImageView
        menu_img = findViewById(R.id.menu_img) as ImageView

        menu_img.visibility = View.GONE

        cat_list.clear()
        main_cat_list.clear()
        cat_img.clear()

        menu_header.visibility = View.GONE
        view.visibility = View.GONE

        back_btn.setOnClickListener {

            finish()

        }

        address.setText("" + str_address)

        val networkStatus: String? =
            NetworkUtil.getConnectivityStatusString(this@MenuActivity)
        if (!networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
            StoreCatApi().execute()
        } else {
//                        if (language.equals("En", ignoreCase = true)) {
            Toast.makeText(
                getApplicationContext(),
                R.string.str_connection_error,
                Toast.LENGTH_SHORT
            ).show()
//                        } else {
//                            Toast.makeText(
//                                getApplicationContext(),
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
        }

    }

    private fun prepareStoreCatJson(): String {

        val mainObj = JSONObject()
        try {
            mainObj.put("UserId", 14)
//            mainObj.put("StoreId", store_id)
            mainObj.put("StoreId", 158)

        } catch (je: JSONException) {
            je.printStackTrace()
        }
        Log.i("TAG", "prepareStoreCatJson: $mainObj")
        return mainObj.toString()
    }

    private inner class StoreCatApi :
        AsyncTask<String?, Int?, String?>() {
        //        ACProgressFlower dialog;
        var customDialog: AlertDialog? = null
        var inputStr: String? = null
        override fun onPreExecute() {
            super.onPreExecute()
            inputStr = prepareStoreCatJson()
            //            dialog = new ACProgressFlower.Builder(DriverListActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(DriverListActivity.this);
            val dialogBuilder =
                context?.let { AlertDialog.Builder(it) }
            // ...Irrelevant code for customizing the buttons and title
            val inflater = layoutInflater
            val layout: Int = R.layout.progress_bar_alert
            val dialogView = inflater.inflate(layout, null)
            dialogBuilder!!.setView(dialogView)
            dialogBuilder.setCancelable(false)
            customDialog = dialogBuilder.create()
            customDialog!!.show()
            val lp = WindowManager.LayoutParams()
            val window = customDialog!!.getWindow()
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            lp.copyFrom(window.attributes)
            //This makes the progressDialog take up the full width
            val display = windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            val screenWidth = size.x
            val d = screenWidth * 0.45
            lp.width = d.toInt()
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            window.attributes = lp
        }

        protected override fun doInBackground(vararg params: String?): String? {
            val apiService: APIInterface = ApiClient.client!!.create(APIInterface::class.java)
            val call: Call<StoreCategoryList?>? = apiService.getStore_category(
                RequestBody.create(MediaType.parse("application/json"), inputStr)
            )
            call!!.enqueue(object : Callback<StoreCategoryList?> {
                override fun onResponse(
                    call: Call<StoreCategoryList?>,
                    response: Response<StoreCategoryList?>
                ) {
                    if (response.isSuccessful()) {
                        val storeCatList: StoreCategoryList? = response.body()
                        if (storeCatList!!.getStatus()) {

                            Log.i("TAG", "onResponse: " + storeCatList.getData()!!.category!!.size)

                            main_cat_list.add("Discover")
                            main_cat_list.add("Hot")
                            main_cat_list.add("Cold")
                            main_cat_list.add("V12")
                            main_cat_list.add("Pastry")
                            main_cat_list.add("Sandwich")
                            main_cat_list.add("Dessert")

                            cat_list.add("Best Selling, New Launch, Offers etc..")
                            cat_list.add("Beverages")
                            cat_list.add("Beverages")
                            cat_list.add("Manual Brew")
                            cat_list.add("Croissants, donut")
                            cat_list.add("Club, Wrab, Panini")
                            cat_list.add("Cheese cake, Regional cake")

                            cat_img.add(R.drawable.add_shot)
                            cat_img.add(R.drawable.hot_drink_menu)
                            cat_img.add(R.drawable.cold_drink_menu)
                            cat_img.add(R.drawable.v12_menu)
                            cat_img.add(R.drawable.pastry_menu)
                            cat_img.add(R.drawable.pastry_menu)
                            cat_img.add(R.drawable.pastry_menu)


                            val linearLayoutManager = LinearLayoutManager(
                                this@MenuActivity,
                                LinearLayoutManager.VERTICAL,
                                false
                            )

                            items_list!!.setLayoutManager(linearLayoutManager)

                            madapter = MenuListAdapter(
                                this@MenuActivity,
                                storeCatList.getData()!!.category,
                                storeCatList.getData()!!.subCategory,
                                str_address
                            )
                            items_list!!.setAdapter(madapter)

                        } else {
                            val failureResponse: String? = storeCatList.getMessageEn()
//                            if (language.equals("En", ignoreCase = true)) {
                            Constants.showOneButtonAlertDialog(
                                failureResponse,
                                context!!.resources.getString(R.string.app_name),
                                context!!.resources.getString(R.string.ok),
                                this@MenuActivity
                            )
//                            } else {
//                                Constants.showOneButtonAlertDialog(
//                                    changePasswordResponse.messageAr,
//                                    context!!.resources.getString(R.string.app_name_ar),
//                                    context!!.resources.getString(R.string.ok_ar),
//                                    this@DriverListActivity
//                                )
//                            }
                        }
                    } else {
                        Log.i("TAG", "onrespon: ")
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.cannot_reach_server,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.cannot_reach_server_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    }
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                }

                override fun onFailure(
                    call: Call<StoreCategoryList?>,
                    t: Throwable
                ) {
                    val networkStatus: String? =
                        NetworkUtil.getConnectivityStatusString(context)
                    if (networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.str_connection_error,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    } else {
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.cannot_reach_server,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.cannot_reach_server_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    }
                    Log.i("TAG", "onFailure: " + t)
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                }
            })
            return null
        }
    }

}