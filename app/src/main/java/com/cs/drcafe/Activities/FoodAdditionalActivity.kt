package com.cs.drcafe.Activities

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Adapters.ItemIndividualAdapter
import com.cs.drcafe.Adapters.RecommendedAdapter
import com.cs.drcafe.R

class FoodAdditionalActivity : AppCompatActivity() {

    lateinit var back_btn: ImageView
    lateinit var list_view: RecyclerView
    lateinit var madapter: RecommendedAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.food_additional)

        back_btn = findViewById(R.id.back_btn) as ImageView
        list_view = findViewById(R.id.list_view) as RecyclerView

        back_btn.setOnClickListener {

            finish()

        }

        val linearLayoutManager1 = LinearLayoutManager(
            this@FoodAdditionalActivity,
            LinearLayoutManager.VERTICAL,
            false
        )
        var mqty : ArrayList<Int> = ArrayList()

        for (i in 0 until 2){
            mqty.add(0)
        }

        list_view!!.setLayoutManager(linearLayoutManager1)
        madapter = RecommendedAdapter(
            this@FoodAdditionalActivity, mqty
        )
        list_view!!.setAdapter(madapter)

    }

}