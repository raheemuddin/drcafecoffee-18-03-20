package com.cs.drcafe.Activities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.cs.bcvendor.Rest.APIInterface
import com.cs.bcvendor.Rest.ApiClient
import com.cs.drcafe.Constants
import com.cs.drcafe.Constants.Companion.lat
import com.cs.drcafe.Constants.Companion.long
import com.cs.drcafe.Constants.Companion.str_address
import com.cs.drcafe.Models.AddressList
import com.cs.drcafe.Models.SaveAddressList
import com.cs.drcafe.Models.UpdateAddressList
import com.cs.drcafe.R
import com.cs.drcafe.Widgets.NetworkUtil
import kotlinx.android.synthetic.main.activity_saveaddress.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddAddressActivity : AppCompatActivity() {

    lateinit var back_btn: ImageView
    lateinit var layout: LinearLayout
    var strLandmark: String = ""
    var strAddress: String = ""
    var strHouseName: String = ""
    var strHouseNumber: String = ""
    var strContactperson: String = ""
    var strContactNo: String = ""
    var strAddressType: String = "Home"
    var int_default: Int = 0
    lateinit var context: Context
    var countryCode: String? = "+966"
    var edit: Boolean = false
    var pos: Int = 0
    lateinit var addresslist: ArrayList<AddressList.dateEntry>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_saveaddress)

        context = this

        back_btn = findViewById(R.id.back_btn) as ImageView
        layout = findViewById(R.id.layout) as LinearLayout

        try {
            edit = intent.getBooleanExtra("edit", false)
            addresslist =
                intent.getSerializableExtra("addresslist") as ArrayList<AddressList.dateEntry>
            pos = intent.getIntExtra("pos", 0)
        } catch (e: Exception) {
            edit = false
        }

        if (edit) {

            strAddressType = addresslist[pos].AddressType
            if (addresslist[pos].AddressType.equals("Home")) {

                img_home.setImageDrawable(resources.getDrawable(R.drawable.address_type_select))
                img_office.setImageDrawable(resources.getDrawable(R.drawable.address_type_unselected))
                img_other.setImageDrawable(resources.getDrawable(R.drawable.address_type_unselected))

            } else if (addresslist[pos].AddressType.equals("Office")) {

                img_home.setImageDrawable(resources.getDrawable(R.drawable.address_type_unselected))
                img_office.setImageDrawable(resources.getDrawable(R.drawable.address_type_select))
                img_other.setImageDrawable(resources.getDrawable(R.drawable.address_type_unselected))

            } else if (addresslist[pos].AddressType.equals("Others")) {

                img_home.setImageDrawable(resources.getDrawable(R.drawable.address_type_unselected))
                img_office.setImageDrawable(resources.getDrawable(R.drawable.address_type_unselected))
                img_other.setImageDrawable(resources.getDrawable(R.drawable.address_type_select))

            }

            txt_address.text = addresslist[pos].Address
            ed_txt_name.setText("" + addresslist[pos].HouseName)
            ed_txt_place.setText("" + addresslist[pos].HouseNo)
            ed_txt_landmark.setText("" + addresslist[pos].LandMark)
            ed_txt_contact_person.setText("" + addresslist[pos].ContactPerson)
            ed_txt_contact_no.setText("+" + addresslist[pos].CountryCode + " " + addresslist[pos].ContactNo)

            if (addresslist[pos].Default == 1) {

                switch_default.isChecked = true

            } else {

                switch_default.isChecked = false

            }

        } else {

            txt_address.text = str_address

            ed_txt_contact_no.setText("" + countryCode)
            ed_txt_contact_no.isCursorVisible = false
        }

        layout_home.setOnClickListener {

            strAddressType = "Home"
            img_home.setImageDrawable(resources.getDrawable(R.drawable.address_type_select))
            img_office.setImageDrawable(resources.getDrawable(R.drawable.address_type_unselected))
            img_other.setImageDrawable(resources.getDrawable(R.drawable.address_type_unselected))
//            if (language.equals("En", ignoreCase = true)) {
            ed_txt_name.setHint(resources.getString(R.string.hint_home))
//            } else {
//                inputHouseNameLayout.setHint(resources.getString(R.string.hint_home_ar))
//            }

        }

        layout_office.setOnClickListener {

            strAddressType = "Office"
            img_home.setImageDrawable(resources.getDrawable(R.drawable.address_type_unselected))
            img_office.setImageDrawable(resources.getDrawable(R.drawable.address_type_select))
            img_other.setImageDrawable(resources.getDrawable(R.drawable.address_type_unselected))

            ed_txt_name.setHint(resources.getString(R.string.hint_office))

        }

        layout_other.setOnClickListener {

            strAddressType = "Others"
            img_home.setImageDrawable(resources.getDrawable(R.drawable.address_type_unselected))
            img_office.setImageDrawable(resources.getDrawable(R.drawable.address_type_unselected))
            img_other.setImageDrawable(resources.getDrawable(R.drawable.address_type_select))

            ed_txt_name.setHint(resources.getString(R.string.hint_other))

        }

        back_btn.setOnClickListener {

            finish()

        }

        layout.setOnClickListener {

            val a = Intent(this, MapViewActivity::class.java)
            startActivity(a)

        }


        ed_txt_place.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable) {

                if (editable.length == 1) {
                    if (editable.toString() == " ") {
                        ed_txt_place.setText("")
                    }
                }
                clearErrors()

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        ed_txt_name.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable) {

                if (editable.length == 1) {
                    if (editable.toString() == " ") {
                        ed_txt_name.setText("")
                    }
                }
                clearErrors()

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        ed_txt_landmark.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable) {

                if (editable.length == 1) {
                    if (editable.toString() == " ") {
                        ed_txt_landmark.setText("")
                    }
                }
                clearErrors()

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        ed_txt_contact_person.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable) {

                if (editable.length == 1) {
                    if (editable.toString() == " ") {
                        ed_txt_contact_person.setText("")
                    }
                }
                clearErrors()

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        ed_txt_contact_no.setOnClickListener {

            ed_txt_contact_no.requestFocus()
            ed_txt_contact_no.isCursorVisible = true

        }

        ed_txt_contact_no.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable) {

                ed_txt_contact_no.isCursorVisible = true
                //                    if (language.equalsIgnoreCase("En")) {
                var enteredMobile = editable.toString()
                if (!enteredMobile.contains(Constants.Country_Code)) {
                    if (enteredMobile.length > Constants.Country_Code.length) {
                        enteredMobile =
                            enteredMobile.substring(Constants.Country_Code.length - 1)
                        ed_txt_contact_no.setText(Constants.Country_Code.toString() + enteredMobile)
                    } else {
                        ed_txt_contact_no.setText(Constants.Country_Code)
                    }
                    ed_txt_contact_no.setSelection(ed_txt_contact_no.length())
                }
                clearErrors()

            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        save.setOnClickListener {

            if (txt_address.length() > 0) {

                if (validation()) {

                    if (edit) {

                        val networkStatus: String? =
                            NetworkUtil.getConnectivityStatusString(this@AddAddressActivity)
                        if (!networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
                            UpdateAddressDetails().execute()
                        } else {
//                        if (language.equals("En", ignoreCase = true)) {
                            Toast.makeText(
                                getApplicationContext(),
                                R.string.str_connection_error,
                                Toast.LENGTH_SHORT
                            ).show()
//                        } else {
//                            Toast.makeText(
//                                getApplicationContext(),
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                        }

                    } else {

                        val networkStatus: String? =
                            NetworkUtil.getConnectivityStatusString(this@AddAddressActivity)
                        if (!networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
                            SaveAddressDetails().execute()
                        } else {
//                        if (language.equals("En", ignoreCase = true)) {
                            Toast.makeText(
                                getApplicationContext(),
                                R.string.str_connection_error,
                                Toast.LENGTH_SHORT
                            ).show()
//                        } else {
//                            Toast.makeText(
//                                getApplicationContext(),
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                        }

                    }
                }

            } else {
//            if (language.equals("En", ignoreCase = true)) {
                Toast.makeText(
                    this,
                    resources.getString(R.string.cannot_reach_server),
                    Toast.LENGTH_SHORT
                ).show()
//            } else {
//                Toast.makeText(
//                    this,
//                    resources.getString(R.string.cannot_reach_server_ar),
//                    Toast.LENGTH_SHORT
//                ).show()
//            }
            }
        }

    }

    fun validation(): Boolean {
        strLandmark = ed_txt_landmark.getText().toString()
        strAddress = txt_address.getText().toString()
        strHouseName = ed_txt_name.getText().toString()
        strHouseNumber = ed_txt_place.getText().toString()
        strContactperson = ed_txt_contact_person.getText().toString()
        if (switch_default.isChecked) {
            int_default = 1
        } else {
            int_default = 0
        }
        strContactNo = ed_txt_contact_no.getText().toString()
        strContactNo = strContactNo.replace("+966 ", "")
        if (strHouseNumber.length == 0) {
//            if (language.equals("En", ignoreCase = true)) {
            input_layout_Place.setError(resources.getString(R.string.error_house_number))
//            } else {
//                inputHouseNumberLayout.setError(resources.getString(R.string.error_house_number_ar))
//            }
            Constants.requestEditTextFocus(input_layout_Place, this@AddAddressActivity)
            return false
        } else if (strLandmark.length == 0) {
//            if (language.equals("En", ignoreCase = true)) {
            input_layout_Landmark.setError(resources.getString(R.string.error_landmark))
//            } else {
//                inputLandmarkLayout.setError(resources.getString(R.string.error_landmark_ar))
//            }
            Constants.requestEditTextFocus(input_layout_Landmark, this@AddAddressActivity)
            return false
        } else if (strHouseName.length == 0) {
//            if (language.equals("En", ignoreCase = true)) {
            input_layout_Name.setError(resources.getString(R.string.error_save_as))
//            } else {
//                inputHouseNameLayout.setError(resources.getString(R.string.error_save_as_ar))
//            }
            Constants.requestEditTextFocus(input_layout_Name, this@AddAddressActivity)
            return false
        } else if (strContactperson.length != 0 || strContactNo.length != 0) {
            if (strContactperson.length == 0) {
//                if (language.equals("En", ignoreCase = true)) {
                input_layout_contact_person.setError(resources.getString(R.string.error_contact_person))
//                } else {
//                    inputContactPersonLayout.setError(resources.getString(R.string.error_contact_person))
//                }
                Constants.requestEditTextFocus(input_layout_contact_person, this@AddAddressActivity)
                return false
            } else if (strContactNo.length == 0) {
//                if (language.equals("En", ignoreCase = true)) {
                input_layout_contact_no.setError(resources.getString(R.string.error_contact_no))
//                } else {
//                    inputContactNoLayout.setError(resources.getString(R.string.error_contact_no))
//                }
                Constants.requestEditTextFocus(input_layout_contact_no, this@AddAddressActivity)
                return false
            } else if (strContactNo.length != 9) {
//                if (language.equals("En", ignoreCase = true)) {
                input_layout_contact_no.setError(resources.getString(R.string.error_contact_no_validation))
//                } else {
//                    inputContactNoLayout.setError(resources.getString(R.string.signup_msg_invalid_mobile_ar))
//                }
                Constants.requestEditTextFocus(input_layout_contact_no, this@AddAddressActivity)
                return false
            }
        }
        return true
    }

    fun clearErrors() {
        input_layout_Name.setErrorEnabled(false)
        input_layout_Landmark.setErrorEnabled(false)
        input_layout_Place.setErrorEnabled(false)
        input_layout_contact_person.setErrorEnabled(false)
        input_layout_contact_no.setErrorEnabled(false)
    }

    private fun prepareSaveAddressJson(): String {
        var id: String? = "0"
        if (edit) {
            try {
                id = addresslist[pos].Id.toString()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            lat = addresslist[pos].Latitude.toDouble()
            long = addresslist[pos].Longitude.toDouble()
        }

        val mainObj = JSONObject()
        try {
            if (edit) {
                mainObj.put("Id", id)
            }
            mainObj.put("UserId", "14")
            mainObj.put("HouseNo", strHouseNumber)
            mainObj.put("HouseName", strHouseName)
            mainObj.put("Address", strAddress)
            mainObj.put("AddressType", strAddressType)
            mainObj.put("CountryCode", "966")
            mainObj.put("Default", int_default)
            mainObj.put("LandMark", strLandmark)
            mainObj.put("Latitude", lat)
            mainObj.put("Longitude", long)
            mainObj.put("ContactPerson", strContactperson)
            mainObj.put("ContactNo", strContactNo)

        } catch (je: JSONException) {
            je.printStackTrace()
        }
        Log.i("TAG", "prepareSaveAddressJson: $mainObj")
        return mainObj.toString()
    }

    private inner class SaveAddressDetails :
        AsyncTask<String?, Int?, String?>() {
        //        ACProgressFlower dialog;
        var customDialog: AlertDialog? = null
        var inputStr: String? = null
        override fun onPreExecute() {
            super.onPreExecute()
            inputStr = prepareSaveAddressJson()
            //            dialog = new ACProgressFlower.Builder(DriverListActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(DriverListActivity.this);
            val dialogBuilder =
                context?.let { AlertDialog.Builder(it) }
            // ...Irrelevant code for customizing the buttons and title
            val inflater = layoutInflater
            val layout: Int = R.layout.progress_bar_alert
            val dialogView = inflater.inflate(layout, null)
            dialogBuilder!!.setView(dialogView)
            dialogBuilder.setCancelable(false)
            customDialog = dialogBuilder.create()
            customDialog!!.show()
            val lp = WindowManager.LayoutParams()
            val window = customDialog!!.getWindow()
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            lp.copyFrom(window.attributes)
            //This makes the progressDialog take up the full width
            val display = windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            val screenWidth = size.x
            val d = screenWidth * 0.45
            lp.width = d.toInt()
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            window.attributes = lp
        }

        protected override fun doInBackground(vararg params: String?): String? {
            val apiService: APIInterface = ApiClient.client!!.create(APIInterface::class.java)
            val call: Call<SaveAddressList?>? = apiService.getSaveAddress(
                RequestBody.create(MediaType.parse("application/json"), inputStr)
            )
            call!!.enqueue(object : Callback<SaveAddressList?> {
                override fun onResponse(
                    call: Call<SaveAddressList?>,
                    response: Response<SaveAddressList?>
                ) {
                    if (response.isSuccessful()) {
                        val saveAddressList: SaveAddressList? = response.body()
                        if (saveAddressList!!.getStatus()) {

                            Log.i("TAG", "onResponse: " + saveAddressList.getMessageEn())
                            finish()

                        } else {
                            val failureResponse: String? = saveAddressList.getMessageEn()
//                            if (language.equals("En", ignoreCase = true)) {
                            Constants.showOneButtonAlertDialog(
                                failureResponse,
                                context!!.resources.getString(R.string.app_name),
                                context!!.resources.getString(R.string.ok),
                                this@AddAddressActivity
                            )
//                            } else {
//                                Constants.showOneButtonAlertDialog(
//                                    changePasswordResponse.messageAr,
//                                    context!!.resources.getString(R.string.app_name_ar),
//                                    context!!.resources.getString(R.string.ok_ar),
//                                    this@DriverListActivity
//                                )
//                            }
                        }
                    } else {
                        Log.i("TAG", "onrespon: ")
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.cannot_reach_server,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.cannot_reach_server_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    }
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                }

                override fun onFailure(
                    call: Call<SaveAddressList?>,
                    t: Throwable
                ) {
                    val networkStatus: String? =
                        NetworkUtil.getConnectivityStatusString(context)
                    if (networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.str_connection_error,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    } else {
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.cannot_reach_server,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.cannot_reach_server_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    }
                    Log.i("TAG", "onFailure: " + t)
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                }
            })
            return null
        }
    }

    private inner class UpdateAddressDetails :
        AsyncTask<String?, Int?, String?>() {
        //        ACProgressFlower dialog;
        var customDialog: AlertDialog? = null
        var inputStr: String? = null
        override fun onPreExecute() {
            super.onPreExecute()
            inputStr = prepareSaveAddressJson()
            //            dialog = new ACProgressFlower.Builder(DriverListActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(DriverListActivity.this);
            val dialogBuilder =
                context?.let { AlertDialog.Builder(it) }
            // ...Irrelevant code for customizing the buttons and title
            val inflater = layoutInflater
            val layout: Int = R.layout.progress_bar_alert
            val dialogView = inflater.inflate(layout, null)
            dialogBuilder!!.setView(dialogView)
            dialogBuilder.setCancelable(false)
            customDialog = dialogBuilder.create()
            customDialog!!.show()
            val lp = WindowManager.LayoutParams()
            val window = customDialog!!.getWindow()
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            lp.copyFrom(window.attributes)
            //This makes the progressDialog take up the full width
            val display = windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            val screenWidth = size.x
            val d = screenWidth * 0.45
            lp.width = d.toInt()
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            window.attributes = lp
        }

        protected override fun doInBackground(vararg params: String?): String? {
            val apiService: APIInterface = ApiClient.client!!.create(APIInterface::class.java)
            val call: Call<UpdateAddressList?>? = apiService.getUpdateAddress(
                RequestBody.create(MediaType.parse("application/json"), inputStr)
            )
            call!!.enqueue(object : Callback<UpdateAddressList?> {
                override fun onResponse(
                    call: Call<UpdateAddressList?>,
                    response: Response<UpdateAddressList?>
                ) {
                    if (response.isSuccessful()) {
                        val updateAddressList: UpdateAddressList? = response.body()
                        if (updateAddressList!!.getStatus()) {

                            Log.i("TAG", "onResponse: " + updateAddressList.getMessageEn())
                            finish()

                        } else {
                            val failureResponse: String? = updateAddressList.getMessageEn()
//                            if (language.equals("En", ignoreCase = true)) {
                            Constants.showOneButtonAlertDialog(
                                failureResponse,
                                context!!.resources.getString(R.string.app_name),
                                context!!.resources.getString(R.string.ok),
                                this@AddAddressActivity
                            )
//                            } else {
//                                Constants.showOneButtonAlertDialog(
//                                    changePasswordResponse.messageAr,
//                                    context!!.resources.getString(R.string.app_name_ar),
//                                    context!!.resources.getString(R.string.ok_ar),
//                                    this@DriverListActivity
//                                )
//                            }
                        }
                    } else {
                        Log.i("TAG", "onrespon: ")
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.cannot_reach_server,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.cannot_reach_server_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    }
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                }

                override fun onFailure(
                    call: Call<UpdateAddressList?>,
                    t: Throwable
                ) {
                    val networkStatus: String? =
                        NetworkUtil.getConnectivityStatusString(context)
                    if (networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.str_connection_error,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    } else {
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.cannot_reach_server,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.cannot_reach_server_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    }
                    Log.i("TAG", "onFailure: " + t)
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                }
            })
            return null
        }
    }

}