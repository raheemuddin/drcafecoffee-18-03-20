package com.cs.drcafe.Activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.drcafe.Adapters.*
import com.cs.drcafe.R

class ItemsindividualActivity : AppCompatActivity() {

    lateinit var back_btn: ImageView
    lateinit var address: TextView
    lateinit var itemslist: RecyclerView
    lateinit var str_address: String
    var food_boolean: Boolean = false
    var menuList: ArrayList<String> = ArrayList()
    var individual_list: ArrayList<String> = ArrayList()
    var sub_items_list: ArrayList<String> = ArrayList()
    var food_items_list: ArrayList<String> = ArrayList()
    var V12sub_items_list: ArrayList<String> = ArrayList()
    var beans_sub_items_list: ArrayList<String> = ArrayList()
    var beans_items_list: ArrayList<String> = ArrayList()
    lateinit var madapter1: ItemIndividualAdapter
    lateinit var madapter2: SubItemListAdapter
    lateinit var madapter3: V12SubItemListAdapter
    lateinit var madapter4: BeansMenuSubItemListAdapter
    lateinit var madapter5: SubItemIndividualAdapter
    lateinit var main_layout: RelativeLayout
    lateinit var title_bar: RelativeLayout
    lateinit var view: View
    lateinit var menu_img: ImageView
    lateinit var transparent_layout: RelativeLayout


    companion object {
        var str_menu_name: String = ""
        var str_menu_name1: String = ""
        lateinit var headerListAdapter: MenuHeaderListAdapter
        lateinit var menu_list: RecyclerView
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item)

        str_address = intent.getStringExtra("address")
        str_menu_name = intent.getStringExtra("str_menu_name")

        menuList.clear()
        individual_list.clear()
        sub_items_list.clear()
        food_items_list.clear()
        V12sub_items_list.clear()
        beans_sub_items_list.clear()
        beans_items_list.clear()

        menuList.add("Featured")
        menuList.add("Best selling")
        menuList.add("Beverage")
        menuList.add("Food")
        menuList.add("V12")
        menuList.add("Beans")

        individual_list.add("Best selling")
        individual_list.add("New Launch")
        individual_list.add("Offers")
        individual_list.add("Coffee of the day")
        individual_list.add("Pastry of the week")

        sub_items_list.add("Coffee of the day")
        sub_items_list.add("Hot Drinks")
        sub_items_list.add("Espresso")
        sub_items_list.add("Espresso with additional")

        food_items_list.add("Donut")
        food_items_list.add("Croissants")
        food_items_list.add("Bagels")
        food_items_list.add("Classic")

        V12sub_items_list.add("Pour Over")
        V12sub_items_list.add("Immersion")
        V12sub_items_list.add("Immersion with pressure")
        V12sub_items_list.add("Cold Brew")

        beans_sub_items_list.add("Private Blend")
        beans_sub_items_list.add("Us Usual")
        beans_sub_items_list.add("Core Green Beans")
        beans_sub_items_list.add("Shaqra")

        beans_items_list.add("FLORAL")
        beans_items_list.add("SWEET / FLORAL")
        beans_items_list.add("SPICY")
        beans_items_list.add("COCOA / NUTTY")
        beans_items_list.add("FRUITY / CITRUSY")

        back_btn = findViewById(R.id.back_btn) as ImageView
        menu_list = findViewById(R.id.menu_list) as RecyclerView
        itemslist = findViewById(R.id.items_list) as RecyclerView
        address = findViewById(R.id.address) as TextView
        main_layout = findViewById(R.id.main_layout) as RelativeLayout
        title_bar = findViewById(R.id.title_bar) as RelativeLayout
        menu_img = findViewById(R.id.menu_img) as ImageView
        transparent_layout = findViewById(R.id.transparent_layout) as RelativeLayout
        view = findViewById(R.id.view) as View

        back_btn.setOnClickListener {

            finish()

        }

        address.setText("" + str_address)

        menu_img.setVisibility(android.view.View.VISIBLE)
        transparent_layout.setVisibility(android.view.View.GONE)

        menu_img.setOnClickListener {

            val a = Intent(this, PopMenu::class.java)
            a.putExtra("address", str_address)
            startActivity(a)
            transparent_layout.setVisibility(android.view.View.VISIBLE)

        }

        val linearLayoutManager = LinearLayoutManager(
            this@ItemsindividualActivity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        menu_list!!.setLayoutManager(linearLayoutManager)

        if (str_menu_name.equals("Featured")) {

            food_boolean = false
            main_layout.setBackgroundColor(resources.getColor(R.color.gray))
            title_bar.setBackgroundColor(resources.getColor(R.color.white))
            back_btn.setImageDrawable(resources.getDrawable(R.drawable.back_arrow))
            address.setTextColor(resources.getColor(R.color.text_color))
            val linearLayoutManager1 = LinearLayoutManager(
                this@ItemsindividualActivity,
                LinearLayoutManager.VERTICAL,
                false
            )

//            menu_list.visibility = View.GONE
//            view.visibility = View.GONE

            str_menu_name1 = individual_list[0]
            headerListAdapter = MenuHeaderListAdapter(
                this@ItemsindividualActivity,
                individual_list
            )
            menu_list!!.setAdapter(headerListAdapter)

            itemslist!!.setLayoutManager(linearLayoutManager1)
            madapter5 = SubItemIndividualAdapter(
                this@ItemsindividualActivity,
                individual_list, food_boolean
            )
            itemslist!!.setAdapter(madapter5)
        } else if (str_menu_name.equals("Beverage")) {

            main_layout.setBackgroundColor(resources.getColor(R.color.gray))
            title_bar.setBackgroundColor(resources.getColor(R.color.white))
            back_btn.setImageDrawable(resources.getDrawable(R.drawable.back_arrow))
            address.setTextColor(resources.getColor(R.color.text_color))

            val linearLayoutManager2 = LinearLayoutManager(
                this@ItemsindividualActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
            str_menu_name1 = sub_items_list[0]
            headerListAdapter = MenuHeaderListAdapter(
                this@ItemsindividualActivity,
                sub_items_list
            )
            menu_list!!.setAdapter(headerListAdapter)
            itemslist!!.setLayoutManager(linearLayoutManager2)
            madapter2 = SubItemListAdapter(
                this@ItemsindividualActivity,
                sub_items_list
            )
            itemslist!!.setAdapter(madapter2)
        } else if (str_menu_name.equals("V12")) {

            main_layout.setBackgroundColor(resources.getColor(R.color.white))
            title_bar.setBackgroundColor(resources.getColor(R.color.v12header))
            back_btn.setImageDrawable(resources.getDrawable(R.drawable.back_arrow_white))
            address.setTextColor(resources.getColor(R.color.white))

            str_menu_name1 = V12sub_items_list[0]
            val linearLayoutManager2 = LinearLayoutManager(
                this@ItemsindividualActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
            itemslist!!.setLayoutManager(linearLayoutManager2)
            headerListAdapter = MenuHeaderListAdapter(
                this@ItemsindividualActivity,
                V12sub_items_list
            )
            menu_list!!.setAdapter(headerListAdapter)
            madapter3 = V12SubItemListAdapter(
                this@ItemsindividualActivity,
                V12sub_items_list
            )
            itemslist!!.setAdapter(madapter3)

        } else if (str_menu_name.equals("Food")) {

            food_boolean = true
            main_layout.setBackgroundColor(resources.getColor(R.color.gray))
            title_bar.setBackgroundColor(resources.getColor(R.color.white))
            back_btn.setImageDrawable(resources.getDrawable(R.drawable.back_arrow))
            address.setTextColor(resources.getColor(R.color.text_color))
            val linearLayoutManager1 = LinearLayoutManager(
                this@ItemsindividualActivity,
                LinearLayoutManager.VERTICAL,
                false
            )

//            menu_list.visibility = View.GONE
//            view.visibility = View.GONE

            str_menu_name1 = food_items_list[0]
            headerListAdapter = MenuHeaderListAdapter(
                this@ItemsindividualActivity,
                food_items_list
            )
            menu_list!!.setAdapter(headerListAdapter)

            itemslist!!.setLayoutManager(linearLayoutManager1)
            madapter5 = SubItemIndividualAdapter(
                this@ItemsindividualActivity,
                food_items_list, food_boolean
            )
            itemslist!!.setAdapter(madapter5)

        } else if (str_menu_name.equals("Beans")) {

            main_layout.setBackgroundColor(resources.getColor(R.color.gray))
            title_bar.setBackgroundColor(resources.getColor(R.color.white))
            back_btn.setImageDrawable(resources.getDrawable(R.drawable.back_arrow))
            address.setTextColor(resources.getColor(R.color.text_color))
            address.setText("Beans")

            str_menu_name1 = beans_sub_items_list[0]
            val linearLayoutManager2 = LinearLayoutManager(
                this@ItemsindividualActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
            itemslist!!.setLayoutManager(linearLayoutManager2)
            headerListAdapter = MenuHeaderListAdapter(
                this@ItemsindividualActivity,
                beans_sub_items_list
            )
            menu_list!!.setAdapter(headerListAdapter)
            madapter4 = BeansMenuSubItemListAdapter(
                this@ItemsindividualActivity,
                beans_sub_items_list, beans_items_list
            )
            itemslist!!.setAdapter(madapter4)
        }

        LocalBroadcastManager.getInstance(this@ItemsindividualActivity).registerReceiver(
            ChangeAdapter, IntentFilter("ChangeAdapter")
        )

    }

    private val ChangeAdapter: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(
            context: Context,
            intent: Intent
        ) {

            if (str_menu_name.equals("Featured")) {

                food_boolean = false
                main_layout.setBackgroundColor(resources.getColor(R.color.gray))
                title_bar.setBackgroundColor(resources.getColor(R.color.white))
                back_btn.setImageDrawable(resources.getDrawable(R.drawable.back_arrow))
                address.setTextColor(resources.getColor(R.color.text_color))

                val linearLayoutManager1 = LinearLayoutManager(
                    this@ItemsindividualActivity,
                    LinearLayoutManager.VERTICAL,
                    false
                )

//            menu_list.visibility = View.GONE
//            view.visibility = View.GONE

                itemslist!!.setLayoutManager(linearLayoutManager1)
                madapter5 = SubItemIndividualAdapter(
                    this@ItemsindividualActivity,
                    individual_list, food_boolean
                )
                itemslist!!.setAdapter(madapter5)
            } else if (str_menu_name.equals("Beverage")) {

                main_layout.setBackgroundColor(resources.getColor(R.color.gray))
                title_bar.setBackgroundColor(resources.getColor(R.color.white))
                back_btn.setImageDrawable(resources.getDrawable(R.drawable.back_arrow))
                address.setTextColor(resources.getColor(R.color.text_color))

                val linearLayoutManager2 = LinearLayoutManager(
                    this@ItemsindividualActivity,
                    LinearLayoutManager.VERTICAL,
                    false
                )
                itemslist!!.setLayoutManager(linearLayoutManager2)
                madapter2 = SubItemListAdapter(
                    this@ItemsindividualActivity,
                    sub_items_list
                )
                itemslist!!.setAdapter(madapter2)
            } else if (str_menu_name.equals("V12")) {

                main_layout.setBackgroundColor(resources.getColor(R.color.white))
                title_bar.setBackgroundColor(resources.getColor(R.color.v12header))
                back_btn.setImageDrawable(resources.getDrawable(R.drawable.back_arrow_white))
                address.setTextColor(resources.getColor(R.color.white))

                val linearLayoutManager2 = LinearLayoutManager(
                    this@ItemsindividualActivity,
                    LinearLayoutManager.VERTICAL,
                    false
                )
                itemslist!!.setLayoutManager(linearLayoutManager2)
                madapter3 = V12SubItemListAdapter(
                    this@ItemsindividualActivity,
                    V12sub_items_list
                )
                itemslist!!.setAdapter(madapter3)
            }else if (str_menu_name.equals("Food")) {

                food_boolean = true
                main_layout.setBackgroundColor(resources.getColor(R.color.gray))
                title_bar.setBackgroundColor(resources.getColor(R.color.white))
                back_btn.setImageDrawable(resources.getDrawable(R.drawable.back_arrow))
                address.setTextColor(resources.getColor(R.color.text_color))
                val linearLayoutManager1 = LinearLayoutManager(
                    this@ItemsindividualActivity,
                    LinearLayoutManager.VERTICAL,
                    false
                )

                itemslist!!.setLayoutManager(linearLayoutManager1)
                madapter5 = SubItemIndividualAdapter(
                    this@ItemsindividualActivity,
                    food_items_list, food_boolean
                )
                itemslist!!.setAdapter(madapter5)

            } else if (str_menu_name.equals("Beans")) {

                main_layout.setBackgroundColor(resources.getColor(R.color.gray))
                title_bar.setBackgroundColor(resources.getColor(R.color.white))
                back_btn.setImageDrawable(resources.getDrawable(R.drawable.back_arrow))
                address.setTextColor(resources.getColor(R.color.text_color))
                address.setText("Beans")

                val linearLayoutManager2 = LinearLayoutManager(
                    this@ItemsindividualActivity,
                    LinearLayoutManager.VERTICAL,
                    false
                )
                itemslist!!.setLayoutManager(linearLayoutManager2)
                madapter4 = BeansMenuSubItemListAdapter(
                    this@ItemsindividualActivity,
                    beans_sub_items_list, beans_items_list
                )
                itemslist!!.setAdapter(madapter4)
            }

        }
    }

}