package com.cs.drcafe.Activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import com.cs.drcafe.R

class PopMenu: Activity() {

    lateinit var cancel: TextView
    lateinit var hot_beverage: LinearLayout
    lateinit var cold_beverage: LinearLayout
    lateinit var v12: LinearLayout
    lateinit var pastry: LinearLayout
    lateinit var sandwich: LinearLayout
    lateinit var dessert: LinearLayout
    lateinit var snacks: LinearLayout
    lateinit var promotion: LinearLayout
    lateinit var str_address: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pop_menu)

        str_address = intent.getStringExtra("address")

        cancel = findViewById(R.id.cancel) as TextView
        hot_beverage = findViewById(R.id.hot_beverage_layout) as LinearLayout
        cold_beverage = findViewById(R.id.cold_beverage_layout) as LinearLayout
        v12 = findViewById(R.id.v12_layout) as LinearLayout
        pastry = findViewById(R.id.pastry_layout) as LinearLayout
        sandwich = findViewById(R.id.sandwich_layout) as LinearLayout
        dessert = findViewById(R.id.dessert_layout) as LinearLayout
        snacks = findViewById(R.id.snacks_layout) as LinearLayout
        promotion = findViewById(R.id.promo_layout) as LinearLayout

        cancel.setOnClickListener {

            finish()

        }

        hot_beverage.setOnClickListener {

            val a = Intent(this, ItemsindividualActivity::class.java)
            a.putExtra("address", str_address)
            a.putExtra("str_menu_name", "Beverage")
            startActivity(a)
            finish()

        }

        cold_beverage.setOnClickListener {

            val a = Intent(this, ItemsindividualActivity::class.java)
            a.putExtra("address", str_address)
            a.putExtra("str_menu_name", "Beverage")
            startActivity(a)
            finish()

        }

        v12.setOnClickListener {

            val a = Intent(this, ItemsindividualActivity::class.java)
            a.putExtra("address", str_address)
            a.putExtra("str_menu_name", "V12")
            startActivity(a)
            finish()

        }

        pastry.setOnClickListener {

            val a = Intent(this, ItemsindividualActivity::class.java)
            a.putExtra("address", str_address)
            a.putExtra("str_menu_name", "Food")
            startActivity(a)
            finish()

        }

        sandwich.setOnClickListener {

            val a = Intent(this, ItemsindividualActivity::class.java)
            a.putExtra("address", str_address)
            a.putExtra("str_menu_name", "Beans")
            startActivity(a)
            finish()

        }


    }

}