package com.cs.drcafe.Activities

import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.drcafe.Adapters.StoreListAdapter
import com.cs.drcafe.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.ui.IconGenerator
import kotlinx.android.synthetic.main.activity_store_map.*
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class StoreActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private var mMap: GoogleMap? = null
    lateinit var marker1: Marker
    lateinit var marker2: Marker
    lateinit var marker3: Marker
    lateinit var marker4: Marker
    lateinit var back_btn: ImageView
    var store_name: ArrayList<String> = ArrayList()
    var store_address: ArrayList<String> = ArrayList()
    lateinit var madapter: StoreListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_map)

        store_name.clear()
        store_address.clear()

        back_btn = findViewById(R.id.back_btn) as ImageView

        back_btn.setOnClickListener {

            finish()

        }

        store_name.add("Ar Rawabi")
        store_name.add("Prince Sultan Bin Abdulaziz")
        store_name.add("Al Imam Saud")
        store_name.add("Al Amir Turki")

        store_address.add("6181 Eastern Ring Branch Rd, AR Rawabi, Riyadh 14215 2475, Saudi Arabia")
        store_address.add("3507 Prince Sultan Bin Abdulaziz Rd, Al Ulaya, Riyadh 12211 7134, Saudi Arabia")
        store_address.add("8078 Al Imam Saud Ibn Abdul Aziz Branch Rd, Al Mursilat, Riyadh 12464, Saudi Arabia")
        store_address.add("6921 Al Amir Turki, Alkurnaish, Al Khobar 34414 2235, Saudi Arabia")


        var mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment!!.getMapAsync(this@StoreActivity)

        img.setOnClickListener {

            if (map_layout.visibility == View.VISIBLE) {

                img.setImageDrawable(resources.getDrawable(R.drawable.store_map))
                map_layout.visibility = View.GONE
                map_list.visibility = View.VISIBLE

            } else {

                img.setImageDrawable(resources.getDrawable(R.drawable.store_list))
                map_layout.visibility = View.VISIBLE
                map_list.visibility = View.GONE

            }

        }

//        val linearLayoutManager2 = LinearLayoutManager(
//            this@StoreActivity,
//            LinearLayoutManager.VERTICAL,
//            false
//        )
//        map_list.setLayoutManager(linearLayoutManager2)
//        madapter = StoreListAdapter(
//            this@StoreActivity,
//            store_name, store_address
//        )
//        map_list!!.setAdapter(madapter)


    }

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0;

        marker1 = mMap!!.addMarker(
            MarkerOptions().position(
                LatLng(24.6813850402832!!, 46.777904510498!!)
            ).snippet("6181 Eastern Ring Branch Rd, AR Rawabi, Riyadh 14215 2475, Saudi Arabia").title("").icon(
                BitmapDescriptorFactory.fromResource(R.drawable.store_map_marker)
            )
        )
        marker2 = mMap!!.addMarker(
            MarkerOptions().position(LatLng(24.6897106170654, 46.6865158081055)).title("").icon(
                BitmapDescriptorFactory.fromResource(R.drawable.store_map_marker)
            )
        )
        marker3 = mMap!!.addMarker(
            MarkerOptions().position(LatLng(24.7557373046875, 46.6833953857422)).title("").icon(
                BitmapDescriptorFactory.fromResource(R.drawable.store_map_marker_12)
            )
        )
        marker4 = mMap!!.addMarker(
            MarkerOptions().position(LatLng(26.2854537963867, 50.2199478149414)).title("").icon(
                BitmapDescriptorFactory.fromResource(R.drawable.store_map_marker_12)
            )
        )
        mMap!!.moveCamera(
            CameraUpdateFactory.newLatLng(
                LatLng(
                    24.6813850402832!!,
                    46.777904510498!!
                )
            )
        )
        mMap!!.animateCamera(CameraUpdateFactory.zoomTo(12.0f))

        mMap!!.setOnMarkerClickListener(this)

    }

    override fun onMarkerClick(p0: Marker?): Boolean {

        if (p0!!.equals(marker1)) {

            val mLocation = Location("")
            mLocation.latitude = marker1.position.latitude
            mLocation.longitude = marker1.position.longitude

            var geocoder: Geocoder? = null
            try {
                geocoder = Geocoder(this, Locale.getDefault())
            } catch (e: Exception) {
                e.printStackTrace()
            }

            // Address found using the Geocoder.
            // Address found using the Geocoder.
            var addresses: List<Address>? = null
            var errorMessage = ""

            try {
                addresses = geocoder!!.getFromLocation(
                    mLocation.latitude,
                    mLocation.longitude,  // In this sample, we get just a single address.
                    1
                )
            } catch (ioException: IOException) { // Catch network or other I/O problems.
                errorMessage = this.getString(R.string.service_not_available)
                Log.e("TAG", errorMessage, ioException)
            } catch (illegalArgumentException: IllegalArgumentException) { // Catch invalid latitude or longitude values.
                errorMessage = this.getString(R.string.invalid_lat_long_used)
                Log.e(
                    "TAG", errorMessage + ". " +
                            "Latitude = " + mLocation.latitude +
                            ", Longitude = " + mLocation.longitude, illegalArgumentException
                )
            }

            if (addresses == null || addresses.size == 0) {
                if (errorMessage.isEmpty()) {
                    errorMessage = getString(R.string.no_address_found)
                    Log.e("TAG", errorMessage)
                }
            } else {
                val address = addresses[0]
                Log.d("TAG", "getLocality: $address")
                //                    if (address.getSubLocality() != null) {
//                        localityTitle.setText(address.getSubLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getSubLocality());
//                    }
//                    else {
//                        localityTitle.setText(address.getLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getLocality());
//                    }

//                mbubble_text.setText("" + address.getAddressLine(0))
//                val a = Intent(this, MenuActivity::class.java)
//                a.putExtra("address", address.getAddressLine(0))
//                startActivity(a)
                Log.d("TAG", "getLocality: " + address.getAddressLine(0))

                val a = Intent(this, MenuActivity::class.java)
                a.putExtra("address", address.getAddressLine(0))
                startActivity(a)

                //Set prevMarker back to default color
//                val iconFactory = IconGenerator(this)
//                iconFactory.setRotation(0)
//                iconFactory.setBackground(null)
//                val view =
//                    View.inflate(this, R.layout.custom_dialog, null)
//                var layout: LinearLayout
//                var store_name1: TextView
//                var store_address1: TextView
//
//
//                layout = view.findViewById(R.id.layout) as LinearLayout
//
//                store_name1 = view.findViewById(R.id.store_name) as TextView
//                store_address1 = view.findViewById(R.id.store_address) as TextView
//
//                store_name1.setText("" + store_name[0])
//                store_address1.setText("" + store_address[0])
//
//                layout.setOnClickListener {
//                    val a = Intent(this, MenuActivity::class.java)
//                    a.putExtra("address", address.getAddressLine(0))
//                    startActivity(a)
//                }
//
//
//                iconFactory.setContentView(view)

            }

        } else if (p0!!.equals(marker2)) {

            val mLocation = Location("")
            mLocation.latitude = marker2.position.latitude
            mLocation.longitude = marker2.position.longitude

            var geocoder: Geocoder? = null
            try {
                geocoder = Geocoder(this, Locale.getDefault())
            } catch (e: Exception) {
                e.printStackTrace()
            }

            // Address found using the Geocoder.
            // Address found using the Geocoder.
            var addresses: List<Address>? = null
            var errorMessage = ""

            try {
                addresses = geocoder!!.getFromLocation(
                    mLocation.latitude,
                    mLocation.longitude,  // In this sample, we get just a single address.
                    1
                )
            } catch (ioException: IOException) { // Catch network or other I/O problems.
                errorMessage = this.getString(R.string.service_not_available)
                Log.e("TAG", errorMessage, ioException)
            } catch (illegalArgumentException: IllegalArgumentException) { // Catch invalid latitude or longitude values.
                errorMessage = this.getString(R.string.invalid_lat_long_used)
                Log.e(
                    "TAG", errorMessage + ". " +
                            "Latitude = " + mLocation.latitude +
                            ", Longitude = " + mLocation.longitude, illegalArgumentException
                )
            }

            if (addresses == null || addresses.size == 0) {
                if (errorMessage.isEmpty()) {
                    errorMessage = getString(R.string.no_address_found)
                    Log.e("TAG", errorMessage)
                }
            } else {
                val address = addresses[0]
                Log.d("TAG", "getLocality: $address")
                //                    if (address.getSubLocality() != null) {
//                        localityTitle.setText(address.getSubLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getSubLocality());
//                    }
//                    else {
//                        localityTitle.setText(address.getLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getLocality());
//                    }

//                mbubble_text.setText("" + address.getAddressLine(0))
                //Set prevMarker back to default color
                val iconFactory = IconGenerator(this)
                iconFactory.setRotation(0)
                iconFactory.setBackground(null)
                val view =
                    View.inflate(this, R.layout.custom_dialog, null)
                var layout: LinearLayout
                var store_name1: TextView
                var store_address1: TextView


                layout = view.findViewById(R.id.layout) as LinearLayout

                store_name1 = view.findViewById(R.id.store_name) as TextView
                store_address1 = view.findViewById(R.id.store_address) as TextView

                store_name1.setText("" + store_name[1])
                store_address1.setText("" + store_address[1])

                layout.setOnClickListener {
                    val a = Intent(this, MenuActivity::class.java)
                    a.putExtra("address", address.getAddressLine(0))
                    startActivity(a)
                }


                iconFactory.setContentView(view)
                Log.d("TAG", "getLocality: " + address.getAddressLine(0))
            }

        } else if (p0!!.equals(marker3)) {

            val mLocation = Location("")
            mLocation.latitude = marker3.position.latitude
            mLocation.longitude = marker3.position.longitude

            var geocoder: Geocoder? = null
            try {
                geocoder = Geocoder(this, Locale.getDefault())
            } catch (e: Exception) {
                e.printStackTrace()
            }

            // Address found using the Geocoder.
            // Address found using the Geocoder.
            var addresses: List<Address>? = null
            var errorMessage = ""

            try {
                addresses = geocoder!!.getFromLocation(
                    mLocation.latitude,
                    mLocation.longitude,  // In this sample, we get just a single address.
                    1
                )
            } catch (ioException: IOException) { // Catch network or other I/O problems.
                errorMessage = this.getString(R.string.service_not_available)
                Log.e("TAG", errorMessage, ioException)
            } catch (illegalArgumentException: IllegalArgumentException) { // Catch invalid latitude or longitude values.
                errorMessage = this.getString(R.string.invalid_lat_long_used)
                Log.e(
                    "TAG", errorMessage + ". " +
                            "Latitude = " + mLocation.latitude +
                            ", Longitude = " + mLocation.longitude, illegalArgumentException
                )
            }

            if (addresses == null || addresses.size == 0) {
                if (errorMessage.isEmpty()) {
                    errorMessage = getString(R.string.no_address_found)
                    Log.e("TAG", errorMessage)
                }
            } else {
                val address = addresses[0]
                Log.d("TAG", "getLocality: $address")
                //                    if (address.getSubLocality() != null) {
//                        localityTitle.setText(address.getSubLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getSubLocality());
//                    }
//                    else {
//                        localityTitle.setText(address.getLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getLocality());
//                    }

//                mbubble_text.setText("" + address.getAddressLine(0))
                //Set prevMarker back to default color
                val iconFactory = IconGenerator(this)
                iconFactory.setRotation(0)
                iconFactory.setBackground(null)
                val view =
                    View.inflate(this, R.layout.custom_dialog, null)
                var layout: LinearLayout
                var store_name1: TextView
                var store_address1: TextView


                layout = view.findViewById(R.id.layout) as LinearLayout

                store_name1 = view.findViewById(R.id.store_name) as TextView
                store_address1 = view.findViewById(R.id.store_address) as TextView

                store_name1.setText("" + store_name[2])
                store_address1.setText("" + store_address[2])

                layout.setOnClickListener {
                    val a = Intent(this, MenuActivity::class.java)
                    a.putExtra("address", address.getAddressLine(0))
                    startActivity(a)
                }


                iconFactory.setContentView(view)
                Log.d("TAG", "getLocality: " + address.getAddressLine(0))
            }

        } else if (p0!!.equals(marker4)) {

            val mLocation = Location("")
            mLocation.latitude = marker4.position.latitude
            mLocation.longitude = marker4.position.longitude

            var geocoder: Geocoder? = null
            try {
                geocoder = Geocoder(this, Locale.getDefault())
            } catch (e: Exception) {
                e.printStackTrace()
            }

            // Address found using the Geocoder.
            // Address found using the Geocoder.
            var addresses: List<Address>? = null
            var errorMessage = ""

            try {
                addresses = geocoder!!.getFromLocation(
                    mLocation.latitude,
                    mLocation.longitude,  // In this sample, we get just a single address.
                    1
                )
            } catch (ioException: IOException) { // Catch network or other I/O problems.
                errorMessage = this.getString(R.string.service_not_available)
                Log.e("TAG", errorMessage, ioException)
            } catch (illegalArgumentException: IllegalArgumentException) { // Catch invalid latitude or longitude values.
                errorMessage = this.getString(R.string.invalid_lat_long_used)
                Log.e(
                    "TAG", errorMessage + ". " +
                            "Latitude = " + mLocation.latitude +
                            ", Longitude = " + mLocation.longitude, illegalArgumentException
                )
            }

            if (addresses == null || addresses.size == 0) {
                if (errorMessage.isEmpty()) {
                    errorMessage = getString(R.string.no_address_found)
                    Log.e("TAG", errorMessage)
                }
            } else {
                val address = addresses[0]
                Log.d("TAG", "getLocality: $address")
                //                    if (address.getSubLocality() != null) {
//                        localityTitle.setText(address.getSubLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getSubLocality());
//                    }
//                    else {
//                        localityTitle.setText(address.getLocality());
//                        LocationPrefsEditor.putString("LocationName", address.getLocality());
//                    }

//                mbubble_text.setText("" + address.getAddressLine(0))

                //Set prevMarker back to default color
                val iconFactory = IconGenerator(this)
                iconFactory.setRotation(0)
                iconFactory.setBackground(null)
                val view =
                    View.inflate(this, R.layout.custom_dialog, null)
                var layout: LinearLayout
                var store_name1: TextView
                var store_address1: TextView


                layout = view.findViewById(R.id.layout) as LinearLayout

                store_name1 = view.findViewById(R.id.store_name) as TextView
                store_address1 = view.findViewById(R.id.store_address) as TextView

                store_name1.setText("" + store_name[3])
                store_address1.setText("" + store_address[3])

                layout.setOnClickListener {
                    val a = Intent(this, MenuActivity::class.java)
                    a.putExtra("address", address.getAddressLine(0))
                    startActivity(a)
                }


                iconFactory.setContentView(view)
                Log.d("TAG", "getLocality: " + address.getAddressLine(0))
            }

        }

        return false
    }
}