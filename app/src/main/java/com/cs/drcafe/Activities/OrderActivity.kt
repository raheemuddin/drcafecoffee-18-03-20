package com.cs.drcafe.Activities

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.cs.drcafe.R

class OrderActivity : AppCompatActivity() {

    lateinit var back_btn: ImageView
    lateinit var order_layout: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.order_screen)

        back_btn = findViewById(R.id.back_btn) as ImageView

        order_layout = findViewById(R.id.order_layout) as LinearLayout
        back_btn.setOnClickListener {
            finish()
        }

        order_layout.setOnClickListener {

            val a = Intent(this, StoreActivity::class.java)
            startActivity(a)

        }

    }

}