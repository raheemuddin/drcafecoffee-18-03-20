package com.cs.drcafe.Activities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.bcvendor.Rest.APIInterface
import com.cs.bcvendor.Rest.ApiClient
import com.cs.drcafe.Adapters.AddressListAdapter
import com.cs.drcafe.Constants
import com.cs.drcafe.Models.AddressList
import com.cs.drcafe.R
import com.cs.drcafe.Widgets.NetworkUtil
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddressActivity : AppCompatActivity() {

    lateinit var recycle_address_list : RecyclerView
    lateinit var back_btn : ImageView
    lateinit var add : TextView
    lateinit var madapter : AddressListAdapter
    lateinit var context: Context
    lateinit var addressLists : ArrayList<AddressList.dateEntry>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_address)

        context = this

        recycle_address_list = findViewById(R.id.address_list) as RecyclerView
        back_btn = findViewById(R.id.back_btn) as ImageView
        add = findViewById(R.id.add) as TextView

        add.setOnClickListener {

            val a = Intent(this@AddressActivity, MapViewActivity::class.java)
            startActivity(a)

        }

        val networkStatus: String? =
            NetworkUtil.getConnectivityStatusString(this@AddressActivity)
        if (!networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
            AddressListApi().execute()
        } else {
//                        if (language.equals("En", ignoreCase = true)) {
            Toast.makeText(
                getApplicationContext(),
                R.string.str_connection_error,
                Toast.LENGTH_SHORT
            ).show()
//                        } else {
//                            Toast.makeText(
//                                getApplicationContext(),
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
        }


    }

    private fun prepareAddresslistJson(): String {


        val mainObj = JSONObject()
        try {
//                        mainObj.put("Id", id)
            mainObj.put("UserId", "14")


        } catch (je: JSONException) {
            je.printStackTrace()
        }
        Log.i("TAG", "prepareAddressListJson: $mainObj")
        return mainObj.toString()
    }

    private inner class AddressListApi :
        AsyncTask<String?, Int?, String?>() {
        //        ACProgressFlower dialog;
        var customDialog: AlertDialog? = null
        var inputStr: String? = null
        override fun onPreExecute() {
            super.onPreExecute()
            inputStr = prepareAddresslistJson()
            //            dialog = new ACProgressFlower.Builder(DriverListActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(DriverListActivity.this);
            val dialogBuilder =
                context?.let { AlertDialog.Builder(it) }
            // ...Irrelevant code for customizing the buttons and title
            val inflater = layoutInflater
            val layout: Int = R.layout.progress_bar_alert
            val dialogView = inflater.inflate(layout, null)
            dialogBuilder!!.setView(dialogView)
            dialogBuilder.setCancelable(false)
            customDialog = dialogBuilder.create()
            customDialog!!.show()
            val lp = WindowManager.LayoutParams()
            val window = customDialog!!.getWindow()
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            lp.copyFrom(window.attributes)
            //This makes the progressDialog take up the full width
            val display = windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            val screenWidth = size.x
            val d = screenWidth * 0.45
            lp.width = d.toInt()
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT
            window.attributes = lp
        }

        protected override fun doInBackground(vararg params: String?): String? {
            val apiService: APIInterface = ApiClient.client!!.create(APIInterface::class.java)
            val call: Call<AddressList?>? = apiService.getAddresslist(
                RequestBody.create(MediaType.parse("application/json"), inputStr)
            )
            call!!.enqueue(object : Callback<AddressList?> {
                override fun onResponse(
                    call: Call<AddressList?>,
                    response: Response<AddressList?>
                ) {
                    if (response.isSuccessful()) {
                        val addresslist: AddressList? = response.body()
                        if (addresslist!!.getStatus()) {

                            addressLists = addresslist.getData()!!;

                            val linear_layout = LinearLayoutManager(this@AddressActivity)
                            recycle_address_list.layoutManager = linear_layout

                            madapter = AddressListAdapter(this@AddressActivity, addressLists, activity = this@AddressActivity)
                            recycle_address_list.adapter = madapter

                            Log.i("TAG", "onResponse: " + addresslist.getMessageEn())

                        } else {
                            val failureResponse: String? = addresslist.getMessageEn()
//                            if (language.equals("En", ignoreCase = true)) {
                            Constants.showOneButtonAlertDialog(
                                failureResponse,
                                context!!.resources.getString(R.string.app_name),
                                context!!.resources.getString(R.string.ok),
                                this@AddressActivity
                            )
//                            } else {
//                                Constants.showOneButtonAlertDialog(
//                                    changePasswordResponse.messageAr,
//                                    context!!.resources.getString(R.string.app_name_ar),
//                                    context!!.resources.getString(R.string.ok_ar),
//                                    this@DriverListActivity
//                                )
//                            }
                        }
                    } else {
                        Log.i("TAG", "onrespon: "  )
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.cannot_reach_server,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.cannot_reach_server_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    }
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                }

                override fun onFailure(
                    call: Call<AddressList?>,
                    t: Throwable
                ) {
                    val networkStatus: String? =
                        NetworkUtil.getConnectivityStatusString(context)
                    if (networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.str_connection_error,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    } else {
//                        if (language.equals("En", ignoreCase = true)) {
                        Toast.makeText(
                            context,
                            R.string.cannot_reach_server,
                            Toast.LENGTH_SHORT
                        ).show()
//                        } else {
//                            Toast.makeText(
//                                context,
//                                R.string.cannot_reach_server_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
                    }
                    Log.i("TAG", "onFailure: " + t)
                    if (customDialog != null) {
                        customDialog!!.dismiss()
                    }
                }
            })
            return null
        }
    }

    override fun onResume() {
        super.onResume()

        val networkStatus: String? =
            NetworkUtil.getConnectivityStatusString(this@AddressActivity)
        if (!networkStatus.equals("Not connected to Internet", ignoreCase = true)) {
            AddressListApi().execute()
        } else {
//                        if (language.equals("En", ignoreCase = true)) {
            Toast.makeText(
                getApplicationContext(),
                R.string.str_connection_error,
                Toast.LENGTH_SHORT
            ).show()
//                        } else {
//                            Toast.makeText(
//                                getApplicationContext(),
//                                R.string.str_connection_error_ar,
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
        }

    }

}